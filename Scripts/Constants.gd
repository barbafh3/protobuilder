extends Node2D

const STEP_SIZE: int = 16
const TILE_SIZE: int = 16
const IDLE_POINT: Vector2 = Vector2(0, 0)
const HAULER_CAPACITY: int = 10
const MAXIMUM_BUIDERS: int = 4
const OUTDOOR_IDLE_RADIUS: int = TILE_SIZE
const INDOOR_IDLE_RADIUS: int = OUTDOOR_IDLE_RADIUS / 2

const Resources = {
	"None": "None",
	"Wood": "Wood",
	"Stone": "Stone",
	"Plank": "Plank",
	"StoneBrick": "StoneBrick"
}

const Jobs = {
	"None": "None",
	"Villager": "Villager",
	"Hauler": "Hauler",
	"Builder": "Builder",
	"Woodcutter": "Woodcutter",
	"Carpenter": "Carpenter",
	"Stonecutter": "Stonecutter"
}

const Buildings = {
	"None": "None",
	"Warehouse": "Warehouse",
	"House": "House",
	"WoodcuttersHut": "Woodcutter's Hut",
	"Sawmill": "Sawmill",
	"Quarry": "Quarry"
}

const BuildingCost = {
	"Warehouse": {
		"Wood": 50
	},
	"WoodcuttersHut": {
		"Wood": 35
	},
	"House": {
		"Wood": 60
	},
	"Quarry": {
		"Wood": 45
	},
	"StonemasonHut": {
		"Stone": 30
	},
	"Sawmill": {
		"Wood": 75
	}
}
