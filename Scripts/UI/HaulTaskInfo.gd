extends Control


func connect_to_task(task):
  task.connect("task_ended", self, "on_task_ended")


func on_task_ended(task):
  task.disconnect("task_ended", self, "on_task_ended")
  queue_free()