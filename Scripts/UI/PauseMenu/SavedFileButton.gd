extends TextureButton

signal save_file_selected(file_path)

var save_file_path: String = ""
var is_toggled: bool = false
var main_button = null


func _ready():
	connect("toggled", self, "on_button_toggled")
	GameManager.connect("mouse_clicked", self, "check_if_clicked")


func check_if_clicked(event_local):
	if get_parent().get_parent().visible == true:
		var button_area = Vector2(rect_size.x, rect_size.y)
		var clicked_outside_button = !Rect2(rect_global_position, button_area).has_point(event_local.position)
		var clicked_outside_main_button = true
		if main_button != null:
			var main_button_area = Vector2(main_button.rect_size.x, main_button.rect_size.y)
			clicked_outside_main_button = !Rect2(main_button.rect_global_position, main_button_area).has_point(event_local.position)
		if clicked_outside_button and clicked_outside_main_button:
			pressed = false


func on_button_toggled(button):
	if not is_toggled:
		emit_signal("save_file_selected", save_file_path)
		is_toggled = true
	else:
		emit_signal("save_file_selected", "")
		is_toggled = false


func fill_button_data(file_info):
	save_file_path = file_info.path
	$FileName.text = file_info.name
	$Date.text = file_info.modified
