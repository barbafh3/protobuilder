extends Control

signal saving_finished()

var selected_file: String = ""
var loaded_files: bool = false


func _ready():
	$Grid/ButtonFill/SaveButton.connect("button_down", self, "on_save_game_pressed")
	$Grid/ButtonFill/CancelButton.connect("button_down", self, "on_cancel_pressed")


func _process(delta):
	if visible and not loaded_files:
		load_save_files()
		loaded_files = true


func on_save_game_pressed():
	if selected_file != "":
		GameManager.save_game(selected_file)
		emit_signal("saving_finished")
	else:
		GameManager.save_to_new_file()
		emit_signal("saving_finished")


func on_cancel_pressed():
	visible = false
	loaded_files = false
	selected_file = ""


func receive_selected_save_file(file_path):
	selected_file = file_path
	print("SaveGameUI: Save file received = ", selected_file)


func load_save_files():
	var save_file_list = GameManager.get_save_files()
	var children = $Grid/InnerFill/GridContainer.get_children()
	var file_count: int = 0
	for button in children:
		if file_count < 3:
			if save_file_list.size() > 0 and save_file_list.size() > file_count:
				button.fill_button_data(save_file_list[file_count])
			button.main_button = $Grid/ButtonFill/SaveButton
			if not button.is_connected("save_file_selected", self, "receive_selected_save_file"):
				button.connect("save_file_selected", self, "receive_selected_save_file")
			file_count += 1
		elif file_count == 0:
			pass

