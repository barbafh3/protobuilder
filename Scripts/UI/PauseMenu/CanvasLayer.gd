extends Tween

var pause_ui: Control = null
var game_ui: Control = null
var menu_button: TextureButton = null

var is_paused: bool = false
var tween_running: bool =false

func _ready():
	pause_ui = get_parent().get_node("PauseUI")
	game_ui = get_parent().get_node("GameUI")
	menu_button = game_ui.get_node("GridContainer/SettingsButton")
	pause_ui.visible = false
	pause_ui.modulate.a = 0.0
	GameManager.connect("game_pause_toggle", self, "on_game_pause_toggle")
	connect("tween_completed", self, "on_tween_complete")


func _process(delta):
	if Input.is_action_just_released("disable_ui"):
		game_ui.visible = not game_ui.visible


func on_game_pause_toggle():
	if not tween_running:
		get_tree().paused = not get_tree().paused
		if is_paused:
			tween_running = true
			interpolate_property(pause_ui, "modulate",
				Color(pause_ui.modulate.r, pause_ui.modulate.g, pause_ui.modulate.b, 1),
				Color(pause_ui.modulate.r, pause_ui.modulate.g, pause_ui.modulate.b, 0),
				0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0.0)
			start()
		else:
			tween_running = true
			pause_ui.visible = true
			interpolate_property(pause_ui, "modulate",
				Color(pause_ui.modulate.r, pause_ui.modulate.g, pause_ui.modulate.b, 0),
				Color(pause_ui.modulate.r, pause_ui.modulate.g, pause_ui.modulate.b, 1),
				0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0.0)
			start()


func on_tween_complete(obj, key):
	tween_running = false
	if is_paused:
		pause_ui.visible = false
		is_paused = false
	else:
		is_paused = true
