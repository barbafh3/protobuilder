extends TextureButton

export var main_menu_path: String = ""


func _ready():
	connect("button_up", self, "on_button_pressed")


func on_button_pressed():
	GameManager.change_scene(main_menu_path)