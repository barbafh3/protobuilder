extends Control

signal loading_finished()

var selected_file: String = ""
var loaded_files: bool = false


func _ready():
  var parent_scene = get_parent().get_parent()
  $Grid/ButtonFill/LoadButton.connect("button_down", self, "on_load_game_pressed")
  $Grid/ButtonFill/CancelButton.connect("button_down", self, "on_cancel_pressed")
  if parent_scene.name != "root":
    parent_scene.connect("ui_closed", self, "on_cancel_pressed")


func _process(delta):
  if visible and not loaded_files:
    load_save_files()
    loaded_files = true
  if selected_file == "":
    $Grid/ButtonFill/LoadButton.disabled = true
  else:
    $Grid/ButtonFill/LoadButton.disabled = false


func receive_selected_save_file(file_path):
  print("LoadGameUI: Save file received = ", file_path)
  selected_file = file_path


func on_load_game_pressed():
  if selected_file != "":
    GameManager.load_game(selected_file)
    selected_file = ""
    emit_signal("loading_finished")
    GameManager.emit_signal("game_pause_toggle")
    get_tree().paused = false
  else:
    print("LoadGameUI: No file selected")


func on_cancel_pressed():
  visible = false
  loaded_files = false
  selected_file = ""


func load_save_files():
  var save_file_list = GameManager.get_save_files()
  var children = $Grid/InnerFill/GridContainer.get_children()
  var file_count: int = 0
  for button in children:
    if file_count < 3:
      if save_file_list.size() > 0 and save_file_list.size() > file_count:
        print("LoadGameUI: File = " + String(save_file_list[file_count]))
        button.fill_button_data(save_file_list[file_count])
      button.main_button = $Grid/ButtonFill/LoadButton
      if not button.is_connected("save_file_selected", self, "receive_selected_save_file"):
        button.connect("save_file_selected", self, "receive_selected_save_file")
      file_count += 1

