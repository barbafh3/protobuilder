extends CanvasLayer

signal ui_closed()

var pause_menu = null
var back_to_game_button = null
var load_panel = null
var load_button = null
var load_cancel_button = null
var save_panel = null
var save_button = null
var save_cancel_button = null
var menu_button = null
var quit_button = null
var main_menu_button = null


func _ready():
  pause_menu = $PauseUI/PauseMenu
  load_panel = $PauseUI/LoadGameUI
  save_panel = $PauseUI/SaveGameUI
  menu_button = $GameUI/GridContainer/SettingsButton
  load_cancel_button = $PauseUI/LoadGameUI/Grid/ButtonFill/CancelButton
  save_cancel_button = $PauseUI/SaveGameUI/Grid/ButtonFill/CancelButton
  save_button = $PauseUI/PauseMenu/Grid/SaveGame/Button
  load_button = $PauseUI/PauseMenu/Grid/LoadGame/Button
  back_to_game_button = $PauseUI/PauseMenu/Grid/BackToGame/Button
  quit_button = $PauseUI/PauseMenu/Grid/Quit/Button
  main_menu_button = $PauseUI/PauseMenu/Grid/MainMenu/Button
  main_menu_button.connect("button_down", self, "on_main_menu_pressed")
  back_to_game_button.connect("button_down", self, "on_back_to_game_pressed")
  load_panel.connect("loading_finished", self, "on_load_finished")
  save_panel.connect("saving_finished", self, "on_saving_finished")
  load_button.connect("button_down", self, "on_load_game_pressed")
  load_cancel_button.connect("button_down", self, "on_load_cancel")
  save_cancel_button.connect("button_down", self, "on_save_cancel")
  save_button.connect("button_down", self, "on_save_game_pressed")
  quit_button.connect("button_down", self, "on_quit_pressed")


func _process(delta):
  check_for_pause()


func check_for_pause():
  if Input.is_action_just_released("ui_cancel"):
    menu_button.pressed = not menu_button.pressed
    yield(get_tree().create_timer(0.2), "timeout")
    emit_signal("ui_closed")
    pause_menu.visible = true
    load_panel.visible = false
    save_panel.visible = false


func on_back_to_game_pressed():
  menu_button.pressed = false


func on_load_game_pressed():
  pause_menu.visible = false
  save_panel.visible = false
  load_panel.visible = true


func on_load_finished():
  pause_menu.visible = true
  load_panel.visible = false
  save_panel.visible = false
  get_tree().paused = false


func on_saving_finished():
  pause_menu.visible = true
  load_panel.visible = false
  save_panel.visible = false


func on_load_cancel():
  load_panel.visible = false
  pause_menu.visible = true


func on_save_game_pressed():
  pause_menu.visible = false
  load_panel.visible = false
  save_panel.visible = true


func on_save_cancel():
  save_panel.visible = false
  pause_menu.visible = true


func on_main_menu_pressed():
  GameManager.change_scene("res://Scenes/World/MainMenu.tscn")


func on_quit_pressed():
  get_tree().quit()
