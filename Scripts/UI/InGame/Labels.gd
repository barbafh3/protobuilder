extends Node


func _ready() -> void:
	StorageManager.connect("store_changes_done", self, "update_label")
	GameManager.connect("global_priority_changed", self, "update_label")
	update_label()


func update_label():
	$WoodCount.text = str(StorageManager.get_total_stored_amount(Constants.Resources.Wood))
	$PlankCount.text = str(StorageManager.get_total_stored_amount(Constants.Resources.Plank))
	$StoneCount.text = str(StorageManager.get_total_stored_amount(Constants.Resources.Stone))
	$StoneBrickCount.text = str(StorageManager.get_total_stored_amount(Constants.Resources.StoneBrick))
	$PriorityValue.text = str(GameManager.global_priority)
