extends Position2D

var text : String = ""


func _ready() -> void:
	$Label.text = text
	$Tween.interpolate_property(self, "modulate",
                              Color(modulate.r, modulate.g, modulate.b, modulate.a),
                              Color(modulate.r, modulate.g, modulate.b, 0.0),
                              0.3, Tween.TRANS_LINEAR, Tween.EASE_OUT, 0.7)
	$Tween.start()


func _process(delta: float) -> void:
	position = Vector2(position.x, position.y - delta - 0.2)
	if modulate.a <= 0.0:
		queue_free()


func set_text(_text : String):
	text = _text