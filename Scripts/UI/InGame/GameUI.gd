extends Control

var buildings_button: TextureButton = null
var settings_button: TextureButton = null
var info_button: TextureButton = null
var graphs_button: TextureButton = null

enum Buttons {
	None,
	Settings,
	Buildings,
	Info,
	Graphs
}


func _ready():
	buildings_button = $GridContainer.get_node("BuildingsButton")
	settings_button = $GridContainer.get_node("SettingsButton")
	info_button = $GridContainer.get_node("InfoButton")
	graphs_button = $GridContainer.get_node("GraphsButton")
	settings_button.connect("toggled", self, "on_settings_button_toggled")
	buildings_button.connect("toggled", self, "on_buildings_button_toggled")
	info_button.connect("toggled", self, "on_info_button_toggled")
	graphs_button.connect("toggled", self, "on_graphs_button_toggled")


func _process(delta):
	check_for_hotkeys()


func check_for_hotkeys():
	if Input.is_action_just_released("buildings_menu"):
		on_buildings_button_toggled(not $BuildingButtonPanel.visible)
	if $BuildingButtonPanel.visible:
		if Input.is_action_just_pressed("warehouse"):
			$BuildingButtonPanel.get_node("GridContainer/WarehouseButton").pressed = true


func on_settings_button_toggled(pressed):
	GameManager.emit_signal("game_pause_toggle")
	if pressed:
		release_buttons_except_for(Buttons.Settings)


func on_buildings_button_toggled(pressed):
	$BuildingButtonPanel.visible = pressed
	if pressed:
		release_buttons_except_for(Buttons.Buildings)


func on_info_button_toggled(pressed):
	$TopPanel.visible = pressed
	if pressed:
		release_buttons_except_for(Buttons.Info)


func on_graphs_button_toggled(pressed):
	if pressed:
		release_buttons_except_for(Buttons.Graphs)


func release_buttons_except_for(button):
	if button != Buttons.Buildings:
		buildings_button.pressed = false
	if button != Buttons.Graphs:
		graphs_button.pressed = false
	if button != Buttons.Info:
		info_button.pressed = false
