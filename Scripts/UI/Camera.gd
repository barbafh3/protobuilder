extends Camera2D

export var camera_speed: float = 10.0
export var min_zoom: float = 0.25
export var max_zoom: float = 1.0
export var zoom_modifier: float = 0.01
export var border_distance_buffer: int = 10

var half_height = OS.get_real_window_size().y / 2
var half_width = OS.get_real_window_size().x / 2


func _ready():
  global_position = Vector2(0.0, 0.0)


func _process(delta):
  check_for_zoom()
  check_for_arrow_movement()
  check_for_middle_mouse_movement()


func check_for_zoom():
  var can_zoom_in = zoom.x - zoom_modifier>= min_zoom and zoom.y - zoom_modifier>= min_zoom
  var can_zoom_out = zoom.x + zoom_modifier <= max_zoom and zoom.y + zoom_modifier<= max_zoom
  if Input.is_action_just_released("zoom_in") and can_zoom_in:
    zoom = Vector2(zoom.x - zoom_modifier, zoom.y - zoom_modifier)
  if Input.is_action_just_released("zoom_out") and can_zoom_out:
    zoom = Vector2(zoom.x + zoom_modifier, zoom.y + zoom_modifier)


func check_for_arrow_movement():
  var horizontal: float = \
    Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
  var vertical: float = \
    Input.get_action_strength("move_down") - Input.get_action_strength("move_up")

  var direction: Vector2 = Vector2(horizontal, vertical)
  var distance: Vector2 = direction * camera_speed
  var vector_sum = global_position + distance
  if (vector_sum.x + half_width) > limit_right:
    vector_sum.x -=  border_distance_buffer
  if (vector_sum.x - half_width) < limit_left:
    vector_sum.x += border_distance_buffer
  if (vector_sum.y + half_height) > limit_bottom:
    vector_sum.y -= border_distance_buffer
  if (vector_sum.y - half_width) < limit_top:
    vector_sum.y += border_distance_buffer
  global_position = vector_sum
  # if (new_position.x + half_width <= limit_right
  #     and new_position.x - half_width >= limit_left
  #     and new_position.y - half_height >= limit_top
  #     and new_position.y + half_height<= limit_bottom):
  #   global_position = new_position


func check_for_middle_mouse_movement():
  if Input.is_action_pressed("mid_mouse"):
    position = get_global_mouse_position()