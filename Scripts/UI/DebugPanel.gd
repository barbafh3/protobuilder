extends Control

export var haul_info_scene: PackedScene = null
export var build_info_scene: PackedScene = null


func _process(delta):
  clear_lists()
  refresh_task_manager_info()
  refresh_task_list()
  if Input.is_action_just_pressed("debug_panel"):
    visible = not visible


func  refresh_task_manager_info():
  if TaskManager != null:
    $TaskCount.text = String(TaskManager.get_task_list().size())
    $HaulCount.text = String(TaskManager.get_haul_count())
    $BuildCount.text = String(TaskManager.get_build_count())


func refresh_task_list():
  if TaskManager != null:
    for task in TaskManager.get_task_list():
      if task.has_method("activate_hauler"):
        var instance = haul_info_scene.instance()
        instance.get_node("HaulerCount").text = String(task.active_haulers.size())
        instance.get_node("Resource").text = String(task.resource_type)
        instance.get_node("ResourceCount").text = String(task.total_resource_amount)
        instance.get_node("Reserved").text = String(task.amount_reserved)
        instance.get_node("Origin").text = String(task.resource_origin.name)
        instance.get_node("Destination").text = String(task.requester.name)
        instance.connect_to_task(task)
        $TaskPanel/TabContainer/HaulList/Container.add_child(instance)
      if task.has_method("get_builders_at_location"):
        var instance = build_info_scene.instance()
        instance.get_node("BuilderCount").text = String(task._working_builders)
        instance.get_node("Construction").text = String(task.requested_construction.name)
        $TaskPanel/TabContainer/BuildList/Container.add_child(instance)

func clear_lists():
  var build_list = $TaskPanel/TabContainer/BuildList/Container.get_children()
  var haul_list = $TaskPanel/TabContainer/HaulList/Container.get_children()
  for child in haul_list:
    child.queue_free()
  for child in build_list:
    child.queue_free()
