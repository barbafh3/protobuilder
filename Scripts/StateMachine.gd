extends Node

var current_state
var controller


func _ready() -> void:
	controller = get_parent()


func _process(delta: float) -> void:
	if current_state != null and current_state.has_method("process"):
		current_state.process(delta)


func _physics_process(delta: float) -> void:
	if current_state != null and current_state.has_method("physics_process"):
		current_state.physics_process(delta)


func change_state(state):
	if current_state != null:
		current_state.queue_free()
	current_state = state
	current_state.enter_state(self)
