extends "res://Scripts/Abstracts/GameTask.gd"

var hauler_list: Array = Array()
var active_haulers: Array = Array()
var total_resource_amount: int = 0
var amount_reserved: int = 0
var resource_type: String = ""
var requester: Node2D = null
var resource_origin: Node2D = null

var _required_haulers: int = 0


func _init(priority: float,
      weight: float,
      _resource_type: String,
      amount: int,
      _requester: Node2D,
      _origin: Node2D = null).(priority, weight) -> void:
  requester = _requester
  resource_origin = _origin
  resource_type = _resource_type
  total_resource_amount = amount
  var required_haulers_raw : float = float(total_resource_amount) / float(Constants.HAULER_CAPACITY)
  _required_haulers = ceil(required_haulers_raw)
  name = "Haul"


func start_task():
  var requested_hauler_list: Array = VillagerManager.request_idle_villager_list(Constants.Jobs.Hauler, _required_haulers)
  for hauler in requested_hauler_list:
    if (hauler != null
        and not hauler.is_busy
        and not hauler_list.has(hauler)
        and hauler_list.size() < _required_haulers):
      hauler_list.append(hauler)
  is_active = true
  print("Haul started")
  run_task()


func run_task():
  if amount_reserved > 0 and active_haulers.size() <= 0:
    amount_reserved = 0
  if resource_origin == null:
    if StorageManager.get_total_stored_amount(resource_type) >= total_resource_amount:
      resource_origin = StorageManager.request_source_for_item(resource_type, Constants.HAULER_CAPACITY)
      choose_haul_behaviour()
  elif requester == null:
    var warehouse = StorageManager.request_storage(resource_type, total_resource_amount)
    if warehouse != null:
      choose_haul_behaviour()
  else:
    choose_haul_behaviour()


func choose_haul_behaviour():
  if total_resource_amount > 0 and amount_reserved < total_resource_amount:
    if hauler_list.size() <= 0 and _required_haulers > 0:
      var hauler = VillagerManager.request_idle_villager(Constants.Jobs.Hauler)
      if hauler != null and not hauler.is_busy and not hauler_list.has(hauler):
        hauler_list.append(hauler)
    if hauler_list.size() > 0:
      for hauler in hauler_list:
        if not hauler.is_busy and hauler != null:
          activate_hauler(hauler)
    if total_resource_amount > 0 and amount_reserved < total_resource_amount and _required_haulers <= 0:
      var hauler = VillagerManager.request_idle_villager(Constants.Jobs.Hauler)
      if hauler != null and not hauler.is_busy and not hauler_list.has(hauler):
        hauler_list.append(hauler)
  if total_resource_amount <= 0:
    emit_signal("task_ended", self)


func activate_hauler(hauler):
  var amount_available: int = total_resource_amount - amount_reserved
  hauler.is_busy = true
  hauler.item_type = resource_type
  hauler.set_resource_destination(requester)
  hauler.set_resource_origin(resource_origin)
  if amount_available > hauler.max_capacity:
    hauler.amount_requested = hauler.max_capacity
  else:
    hauler.amount_requested = amount_available
  amount_reserved += hauler.amount_requested
  if not hauler.is_connected("hauling_finished", self, "hauling_finished"):
    hauler.connect("hauling_finished", self, "hauling_finished")
  _required_haulers -= 1
  active_haulers.append(hauler)
  hauler_list.erase(hauler)


func hauling_finished(hauler: Node2D, amount_delivered: int, returned_amount: int = 0):
  if hauler != null and hauler.is_in_group(Constants.Jobs.Hauler):
    if hauler.is_connected("hauling_finished", self, "hauling_finished"):
      hauler.disconnect("hauling_finished", self, "hauling_finished")
  if returned_amount == 0:
    total_resource_amount -= amount_delivered
    amount_reserved -= amount_delivered
  else:
    amount_reserved -= returned_amount
  if active_haulers.has(hauler):
    active_haulers.erase(hauler)


func save_data(file):
  var hauler_id_list: Array = []
  var active_hauler_id_list: Array = []
  var origin

  for hauler in hauler_list:
    hauler_id_list.append(hauler.data_id)
  for hauler in active_haulers:
    active_hauler_id_list.append(hauler.data_id)

  if resource_origin != null:
    origin = resource_origin.data_id
  else:
    origin = null
  var data: Dictionary = {
    "data_id": data_id,
    "type": "task",
    "task_type": "haul",
    "script": "res://Scripts/Tasks/Haul.gd",
    "priority": priority,
    "weight": weight,
    "hauler_list": hauler_id_list,
    "active_haulers": active_hauler_id_list,
    "total_resource_amount": total_resource_amount,
    "resource_type": resource_type,
    "amount_reserved": amount_reserved,
    "requester": requester.data_id,
    "resource_origin": origin,
    "required_haulers": _required_haulers
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  for id in data.hauler_list:
    var hauler = GameManager.get_node_by_id(id)
    hauler_list.append(hauler)
  for id in data.active_haulers:
    var hauler = GameManager.get_node_by_id(id)
    active_haulers.append(hauler)
  amount_reserved = data.amount_reserved
  _required_haulers = data.required_haulers


