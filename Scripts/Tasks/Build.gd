extends "res://Scripts/Abstracts/GameTask.gd"

var max_builders = Constants.MAXIMUM_BUIDERS
var _builders: Array = Array() setget ,get_builders
var requested_construction setget _set_requested_construction,get_requested_construction
var _working_builders: int = 0


func get_builders() -> Array:
	return _builders.duplicate()

func get_requested_construction():
	return requested_construction

func _set_requested_construction(construction):
	requested_construction = construction


func _init(priority : float, weight : float, construction).(priority, weight):
	_set_requested_construction(construction)
	name = "Build"


func start_task():
	var builders = VillagerManager.request_villager_list(Constants.Jobs.Builder, Constants.MAXIMUM_BUIDERS)
	for builder in builders:
		if builder != null and not _builders.has(builder):
			if not builder.is_busy:
				_builders.push_front(builder)
			# elif builder.current_task.weighted_priority < weighted_priority:
			# 	builder.current_task = self
			# 	builder.requested_construction = requested_construction
			# 	_builders.push_front(builder)
	is_active = true
	run_task()


func run_task():
	if requested_construction.construction_remaining > 0:
		if _builders.size() > 0:
			for builder in _builders.duplicate():
				if not _builders[0].is_busy:
					var popped_builder = _builders.pop_front()
					_working_builders += 1
					popped_builder.requested_construction = requested_construction
					# popped_builder.current_task = self
		else:
			var builders_at_location = get_builders_at_location()
			if builders_at_location.size() < Constants.MAXIMUM_BUIDERS:
				var new_builder = VillagerManager.request_villager(Constants.Jobs.Builder)
				if new_builder != null and not _builders.has(new_builder):
					if not new_builder.is_busy:
						_builders.push_front(new_builder)
					# elif new_builder.current_task.weighted_priority < weighted_priority:
					# 	new_builder.current_task = self
					# 	new_builder.requested_construction = requested_construction
					# 	_builders.push_front(new_builder)
	else:
		emit_signal("task_ended", self)


func get_builders_at_location() -> Array:
	var bodies = requested_construction.loading_area.get_overlapping_bodies()
	var list : Array = Array()
	for body in bodies:
		if body.is_in_group("Builder"):
			list.append(body)
	return list


func worker_finished(builder : Node2D):
	builder.disconnect("work_finished", self, "worker_finished")
	_working_builders -= 1


func save_data(file):
	var builder_id_list: Array = []
	for builder in _builders:
		builder_id_list.append(builder.data_id)
	var data = {
		"data_id": data_id,
		"type": "task",
		"task_type": "build",
		"script": "res://Scripts/Tasks/Build.gd",
		"priority": priority,
		"weight": weight,
		"builders": builder_id_list,
		"requested_construction": requested_construction.data_id,
		"working_builders": _working_builders
	}
	file.store_line(to_json(data))


func load_data(data):
	data_id = data.data_id
	for id in data.builders:
		var builder = GameManager.get_node_by_id(id)
		_builders.push_front(builder)
	if data.requested_construction != null:
		requested_construction = GameManager.get_node_by_id(data.requested_construction)
	_working_builders = data.working_builders
