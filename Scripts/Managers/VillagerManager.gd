extends Node2D

var _villager_scenes = {
	Constants.Jobs.Villager: preload("res://Scenes/Characters/Villager.tscn"),
	Constants.Jobs.Hauler: preload("res://Scenes/Characters/Hauler.tscn"),
	Constants.Jobs.Builder: preload("res://Scenes/Characters/Builder.tscn"),
	Constants.Jobs.Woodcutter: preload("res://Scenes/Characters/Woodcutter.tscn"),
	Constants.Jobs.Carpenter: preload("res://Scenes/Characters/Carpenter.tscn"),
	Constants.Jobs.Stonecutter: preload("res://Scenes/Characters/Stonecutter.tscn")
}

var villagers_node_path: String = "/root/Map/Villagers"

var villager_scenes = _villager_scenes.duplicate()
var _villagers : Array = Array()


func clear_villagers():
	_villagers.clear()


func register_villager(villager):
	if not villager in _villagers:
		_villagers.append(villager)


func get_generic_villager(start_position: Vector2):
	var instance = _villager_scenes[Constants.Jobs.Villager].instance()
	instance.global_position = start_position
	get_node(villagers_node_path).add_child(instance)


func request_new_villager(type: String):
	var selected_villager = null
	for villager in _villagers.duplicate():
		if villager.is_in_group(Constants.Jobs.Villager):
			var instance = _villager_scenes[type].instance()
			instance.global_position = villager.global_position
			_villagers.append(instance)
			selected_villager = instance
			get_node(villagers_node_path).add_child(instance)
			_villagers.erase(villager)
			villager.queue_free()
			break
	_villagers.shuffle()
	return selected_villager


func revert_to_villager(villager: Node2D):
	var instance = _villager_scenes[Constants.Jobs.Villager].instance()
	instance.global_position = villager.global_position
	_villagers.append(instance)
	get_node(villagers_node_path).add_child(instance)
	_villagers.erase(villager)
	villager.queue_free()


func request_idle_villager(type : String):
	var selected_villager = null
	for villager in _villagers:
		if not villager.is_busy and villager.is_in_group(type):
			selected_villager = villager
			break
	_villagers.shuffle()
	return selected_villager


func request_villager(type : String):
	var selected_villager = null
	for villager in _villagers:
		if villager.is_in_group(type):
			selected_villager = villager
			break
	_villagers.shuffle()
	return selected_villager


func request_idle_villager_list(type : String, amount : int) -> Array:
	var list = Array()
	if _villagers.size() > 0:
		for villager in _villagers:
			if not villager.is_busy and list.size() <= amount and not list.has(villager) and villager.is_in_group(type):
				list.append(villager)
	_villagers.shuffle()
	return list


func request_villager_list(type : String, amount : int) -> Array:
	var list : Array = Array()
	if _villagers.size() > 0:
		for villager in _villagers:
			if list.size() <= amount and not list.has(villager) and villager.is_in_group(type):
				list.append(villager)
	_villagers.shuffle()
	return list

