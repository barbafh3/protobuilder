extends Node2D

signal storage_items_changed(resource_name, amount)
signal store_changes_done()

var _storage_buildings: Array

var _storage : Dictionary = {
	Constants.Resources.Wood: 0,
	Constants.Resources.Stone: 0,
	Constants.Resources.Plank: 0,
	Constants.Resources.StoneBrick: 0,
}


func _ready() -> void:
		connect("storage_items_changed", self, "update_storage")
		clear_global_storage()


func save_data(file):
	var data = {
		"storage": _storage,
		"storage_buildings": _storage_buildings
	}

	file.store_line(to_json(data))


func clear_global_storage():
	populate_empty_storage()
	_storage_buildings.clear()
	emit_signal("store_changes_done")


func make_populated_storage_dict() -> Dictionary:
	return {
		Constants.Resources.Wood: 0,
		Constants.Resources.Stone: 0,
		Constants.Resources.Plank: 0,
		Constants.Resources.StoneBrick: 0,
	}


func populate_empty_storage():
	_storage = make_populated_storage_dict()


func register_storage(building):
	if not building in _storage_buildings:
		for resource in building.storage:
			if building.storage[resource] > 0:
				update_storage(resource, building.storage[resource])
		_storage_buildings.append(building)


func unregister_storage(building):
	if building in _storage_buildings:
		for resource in building.storage:
			if building.storage[resource] > 0:
				update_storage(resource, -building.storage[resource])
		_storage_buildings.erase(building)


func update_storage(resource_name : String, amount : int):
	_storage[resource_name] += amount
	emit_signal("store_changes_done")


func update_storage_from_dict(storage: Dictionary):
	for key in storage:
		_storage[key] += storage[key]
	emit_signal("store_changes_done")


func get_total_stored_amount(resource_name : String) -> int:
	return _storage[resource_name]


func list_warehouses() -> Array:
	var list: Array = Array()
	for building in _storage_buildings:
		if building.is_in_group("Warehouse"):
			list.append(building)
	return list


func list_stockpiles() -> Array:
	var list: Array = Array()
	for building in _storage_buildings:
		if building.is_in_group("Stockpile"):
			list.append(building)
	return list


func request_storage(resource_name : String, amount : int):
	var selected_storage = null
	for building in _storage_buildings:
		if building.is_in_group("Warehouse"):
			if not building.storage.has(resource_name):
				building.storage[resource_name] = 0
			if (building.storage[resource_name] + amount) <= building.max_capacity:
				selected_storage = building
				break
	return selected_storage


func request_source_for_item(resource_name : String, amount : int):
	var selected_source = null
	for building in _storage_buildings:
		if building.is_in_group("Warehouse") or building.is_in_group("Crate"):
			if not building.storage.has(resource_name):
				building.storage[resource_name] = 0
			if building.storage[resource_name] >= amount:
				var result = building.reserve_resource(resource_name, amount)
				if result:
					selected_source = building
					break
	return selected_source


func get_available_warehouses(required_space : int) -> Array:
	var list: Array = Array()
	if _storage_buildings.size() > 0:
		for building in _storage_buildings:
			if building.is_in_group("Warehouse"):
				var available_space: int = building.max_capacity - building.get_total_usage()
				if available_space >= required_space:
					list.append(building)
	return list
