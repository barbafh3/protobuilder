extends Node2D

signal global_priority_changed()
signal mouse_clicked(event_local)
signal game_pause_toggle()

signal new_building_selected(building)

signal save_tasks_called(file)
signal save_villagers_called(file)
signal save_buildings_called(file)
signal save_resources_called(file)

var scenes = {
  main_menu = "res://Scenes/World/MainMenu.tscn",
  map_1 = "res://Scenes/World/Map1.tscn",
  empty_map = "res://Scenes/World/EmptyMap.tscn"
}

var node_list: Dictionary

var data_folder: String = "user://"
var selected_save_file: String = data_folder + "save_data.sav"

var global_priority: int = 9

var current_mouse_tile: Vector2
var tile_building = null


func _ready():
  pause_mode = Node.PAUSE_MODE_PROCESS
  # get_save_files()


func _input(event):
  if (event is InputEventMouseButton) and event.pressed:
    emit_signal("mouse_clicked", event)


func _process(delta):
  # check_for_pause()
  check_for_priority_changes()
  if Input.is_action_just_released("quick_save"):
    save_game(data_folder + "autosave.sav")
  if Input.is_action_just_released("quick_load"):
    load_game(selected_save_file)


func set_current_mouse_tile(tile: Vector2):
  current_mouse_tile = tile


func set_tile_occupation(building):
  tile_building = building


func check_for_priority_changes():
  if Input.is_action_just_released("priority_up") and global_priority < 9:
    global_priority += 1
    emit_signal("global_priority_changed")
  if Input.is_action_just_released("priority_down") and global_priority > 0:
    global_priority -= 1
    emit_signal("global_priority_changed")


func change_scene(scene: String, reload = true):
    get_tree().paused = false
    if reload:
      StorageManager.clear_global_storage()
      TaskManager.clear_tasks()
      VillagerManager.clear_villagers()
    get_tree().change_scene(scene)


func get_node_by_id(requested_id: String) -> Node2D:
  for id in node_list:
    if id == requested_id:
      return  node_list[id]
  return null


func save_to_new_file():
  var path: String = ""
  var list = get_save_files()
  if list.size() < 3:
    path = data_folder + "SaveData" + String(list.size() + 1) + ".sav"
    var file = File.new()
    file.open(path, File.WRITE)

    emit_signal("save_resources_called", file)
    yield(get_tree().create_timer(0.1), "timeout")
    emit_signal("save_tasks_called", file)
    yield(get_tree().create_timer(0.1), "timeout")
    emit_signal("save_villagers_called", file)
    yield(get_tree().create_timer(0.1), "timeout")
    emit_signal("save_buildings_called", file)

    file.close()
    print("GameManager: Game saved!")
  else:
    print("GameManager: All save slots filled")


func save_game(file_path):
  var file = File.new()
  file.open(file_path, File.WRITE)

  emit_signal("save_resources_called", file)
  yield(get_tree().create_timer(0.1), "timeout")
  emit_signal("save_tasks_called", file)
  yield(get_tree().create_timer(0.1), "timeout")
  emit_signal("save_villagers_called", file)
  yield(get_tree().create_timer(0.1), "timeout")
  emit_signal("save_buildings_called", file)

  file.close()
  print("GameManager: Game saved!")


func load_game(file_path):
  var resources: Array = []
  var buildings: Array = []
  var villagers: Array = []
  var tasks: Array = []
  TaskManager.clear_tasks()
  get_tree().change_scene(scenes.empty_map)
  yield(get_tree().create_timer(0.1), "timeout")
  StorageManager.clear_global_storage()
  VillagerManager.clear_villagers()
  var file = File.new()
  file.open(file_path, File.READ)
  while file.get_position() < file.get_len():
    var data = parse_json(file.get_line())
    match data.type:
      "building":
        buildings.append(data)
      "villager":
        villagers.append(data)
      "resource":
        resources.append(data)
      "task":
        tasks.append(data)
  load_resources(resources)
  load_buildings(buildings)
  load_villagers(villagers)
  load_tasks(tasks)


func load_resources(list):
  for resource in list:
    var node: PackedScene = load(resource.scene)
    var instance: Node = node.instance()
    var id: String = resource.data_id
    node_list[id] = instance
    get_node("/root/Map/Resources").add_child(instance)
    instance.load_data(resource)


func load_buildings(list):
  for building in list:
    var node: PackedScene = load(building.scene)
    var instance: Node = node.instance()
    var id: String = building.data_id
    node_list[id] = instance
    instance.load_data(building)
    get_node("/root/Map/Buildings").add_child(instance)


func load_villagers(list):
  for villager in list:
    var node: PackedScene = load(villager.scene)
    var instance: Node = node.instance()
    var id: String = villager.data_id
    instance.loading_data = villager
    node_list[id] = instance
    get_node("/root/Map/Villagers").add_child(instance)
    instance.load_data(villager)


func load_tasks(list):
  for task in list:
    if task.task_type == "haul":
      create_haul_from_data(task)
    if task.task_type == "build":
      create_build_from_data(task)


func load_task_data():
  for node in node_list.values():
    if node.has_method("load_task_data"):
      node.load_task_data()


func create_haul_from_data(data):
  var destination = GameManager.get_node_by_id(data.requester)
  var origin = null
  if data.resource_origin != null:
    origin = GameManager.get_node_by_id(data.resource_origin)
  var haul = TaskManager.create_haul(data.priority, data.weight, data.resource_type, data.total_resource_amount, destination, origin)
  haul.load_data(data)
  node_list[data.data_id] = haul


func create_build_from_data(data):
  var script = load(data.script)
  var construction = GameManager.get_node_by_id(data.requested_construction)
  var build = script.new(data.priority, data.weight, construction)
  build.load_data(data)
  node_list[data.data_id] = build
  TaskManager.register_task(build)


func get_save_files():
  var folder = "user://"
  var files = []
  var dir = Directory.new()

  dir.open(folder)
  dir.list_dir_begin()
  while true:
    var file = dir.get_next()
    if file == "":
      break
    elif file.ends_with(".sav"):
      print("GameManager: " + String(file) +" added to save list")
      files.append(file)
  dir.list_dir_end()

  var file_info_list: Array = Array()
  var new_file = File.new()
  var file_count: int = 0
  for file in files:
    if file_count < 3:
      var raw_date = OS.get_datetime_from_unix_time(new_file.get_modified_time(data_folder + file))
      var formatted_date = String(raw_date.day) + "/" + String(raw_date.month) + "/" + String(raw_date.year)
      file_info_list.append({ "name": file.get_basename(), "modified": formatted_date, "path": data_folder + file })
      file_count += 1
  print(file_info_list)
  return file_info_list
