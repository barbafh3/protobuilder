extends Node2D

var haul_script = preload("res://Scripts/Tasks/Haul.gd")
var _active_tasks : Array


func _process(delta: float) -> void:
	for task in _active_tasks.duplicate():
		if task.is_active:
			task.run_task()
		else:
			task.start_task()


func get_task_list() -> Array:
  return _active_tasks.duplicate()


func get_haul_count() -> int:
  var count = 0
  for task in _active_tasks:
    if task.has_method("activate_hauler"):
      count += 1
  return count


func get_build_count() -> int:
  var count = 0
  for task in _active_tasks:
    if task.has_method("get_builders_at_location"):
      count += 1
  return count



func create_haul(priority: int, weight: int, resource: String, amount: int, requester: Node2D, origin: Node2D = null):
	var haul = haul_script.new(priority, weight, resource, amount, requester, origin)
	register_task(haul)
	return haul


func register_task(task):
	_active_tasks.append(task)
	if _active_tasks.size() > 1:
		_active_tasks.sort_custom(self, "sort_ascending")
	task.connect("task_ended", self, "close_task")


func sort_ascending(a, b):
	if a.weighted_priority < b.weighted_priority:
		return true
	return false


func close_task(task):
	print("TaskManager: ", task.name, " task ended")
	_active_tasks.erase(task)
	task.queue_free()


func clear_tasks():
	_active_tasks.clear()

