extends Control


func _ready():
	$Grid/Play/Button.connect("button_down", self, "on_play_pressed")
	$Grid/LoadGame/Button.connect("button_down", self, "on_load_pressed")
	$Grid/Quit/Button.connect("button_down", self, "on_quit_pressed")


func on_play_pressed():
	GameManager.change_scene(GameManager.scenes.map_1)


func on_load_pressed():
	$LoadGameUI.visible = true


func on_quit_pressed():
	get_tree().quit()
