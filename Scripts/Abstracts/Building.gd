extends Node2D

signal building_destroyed()

export var required_resources: Dictionary
export var construction_remaining: float = 0
export var is_pre_built: bool = false

enum States {
  None = 0,
  Placing = 1,
  Loading = 2,
  Construction = 3,
  Active = 4,
  Disabled = 5
}

var crate_scene_path = "res://Scenes/Resources/Crate.tscn"
var data_id: String = ""
var loading_data = null
var is_loaded: bool = false
var current_state = States.None
var animation_player: AnimationPlayer = null
var sprite: Sprite = null
var loading_area: Area2D = null
var finished_construction: bool = false
var assigned_priority: float
var placing_ready: bool = false


func _enter_tree():
  if data_id == "":
    data_id = String(get_instance_id())
  GameManager.connect("save_buildings_called", self, "save_data")


func _ready():
  $LoadingArea.connect("area_entered", self, "on_area_entered")
  $LoadingArea.connect("area_exited", self, "on_area_exited")


func on_area_entered(area):
  if area.get_parent().is_in_group("Building"):
    if current_state == States.Active or current_state == States.Disabled:
      print("Building: ", area.name, " entered ", name)
      GameManager.set_tile_occupation(get_parent())


func on_area_exited(area):
  if area.get_parent().is_in_group("Building"):
    if current_state == States.Active or current_state == States.Disabled:
      print("Building: ", area.name, " exited ", name)
      GameManager.set_tile_occupation(null)


func load_data(data):
  pass

func get_new_crate() -> Resource:
  return load(crate_scene_path)


func connect_to_area():
  print("Loading: connected to area")
  loading_area.connect("body_entered", self, "take_resource")


func connect_to_builders():
  if not is_loaded:
    loading_area.disconnect("body_entered", self, "take_resource")
  if not loading_area.is_connected("body_entered", self, "receive_builders"):
    loading_area.connect("body_entered", self, "receive_builders")
  if not loading_area.is_connected("body_exited", self, "remove_builders"):
    loading_area.connect("body_exited", self, "remove_builders")


func disconnect_builders():
  if not is_loaded:
    if loading_area.is_connected("body_entered", self, "receive_builders"):
      loading_area.disconnect("body_entered", self, "receive_builders")
    if loading_area.is_connected("body_exited", self, "remove_builders"):
      loading_area.disconnect("body_exited", self, "remove_builders")


func receive_builders(body: Node2D):
  if body.is_in_group("Builder"):
    body.is_inside_building = true
    body.current_construction = self


func remove_builders(body: Node2D):
  if body.is_in_group("Builder"):
    body.is_inside_building = false
    body.current_construction = null


func do_building_work(amount: float):
  construction_remaining -= amount


func take_resource(body: Node2D):
  if body.is_in_group("Hauler"):
    if body.resource_destination == self:
      var resources = body.deliver_resources()
      required_resources[resources.type] -= resources.amount
      finished_construction = is_construction_finished()


func is_construction_finished() -> bool:
  var result: bool = false
  for key in required_resources:
    if required_resources[key] <= 0:
      result = true
    else:
      result = false
  return result


func change_state(state):
  if state == States.Placing and state != current_state:
    $PlacingState.enter_state(self)
    current_state = state
  if state == States.Loading and state != current_state:
    $LoadingState.enter_state(self)
    current_state = state
  if state == States.Construction and state != current_state:
    $ConstructionState.enter_state(self)
    current_state = state
  if state == States.Active and state != current_state:
    $ActiveState.enter_state(self)
    current_state = state
  if state == States.None and state != current_state:
    current_state = state


func run_state_process(delta):
  if current_state == States.Placing:
    if $PlacingState.has_method("process"):
      $PlacingState.process(delta)
  if current_state == States.Loading:
    if $LoadingState.has_method("process"):
      $LoadingState.process(delta)
  if current_state == States.Construction:
    if $ConstructionState.has_method("process"):
      $ConstructionState.process(delta)
  if current_state == States.Active:
    if $ActiveState.has_method("process"):
      $ActiveState.process(delta)
  if current_state == States.None:
    pass


func run_state_physics_process(delta):
  if current_state == States.Placing:
    if $PlacingState.has_method("physics_process"):
      $PlacingState.physics_process(delta)
  if current_state == States.Loading:
    if $LoadingState.has_method("physics_process"):
      $LoadingState.physics_process(delta)
  if current_state == States.Construction:
    if $ConstructionState.has_method("physics_process"):
      $ConstructionState.physics_process(delta)
  if current_state == States.Active:
    if $ActiveState.has_method("physics_process"):
      $ActiveState.physics_process(delta)
  if current_state == States.None:
    pass


func destroy_and_drop_resources(dropped_resources: Dictionary):
  var crate: Node2D = get_new_crate().instance()
  crate.storage = dropped_resources
  crate.global_position = global_position
  get_node("/root/Map/Resources").add_child(crate)
  StorageManager.register_storage(crate)
  emit_signal("building_destroyed")
  queue_free()

