extends Node2D

signal task_ended(task)

var data_id: String = ""
var loading_data = null

var priority : float
var weight : float
var weighted_priority : float

var is_active : bool = false
var is_running : bool = false

func _init(_priority : float, _weight : float) -> void:
	priority = _priority
	weight = _weight
	weighted_priority = priority * weight
	data_id = String(get_instance_id())
	GameManager.connect("save_tasks_called", self, "save_data")