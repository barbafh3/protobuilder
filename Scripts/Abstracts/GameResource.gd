extends Node2D

signal resource_spent(trees)

export var resource_amount : int = 0

var data_id: String = ""
var current_state = States.None
var reserved_amount: int = 0

enum States {
  None,
  Active,
  Spent
}


func _enter_tree():
  if data_id == "":
    data_id = String(get_instance_id())
  if not GameManager.is_connected("save_resources_called", self, "save_data"):
    GameManager.connect("save_resources_called", self, "save_data")


func _ready():
  connect("area_entered", self, "on_area_entered")
  connect("area_exited", self, "on_area_exited")


func on_area_entered(area):
  if area.get_parent().is_in_group("Building"):
    print("Building: Entered ", get_parent().name)
    GameManager.set_tile_occupation(get_parent())


func on_area_exited(area):
  if area.get_parent().is_in_group("Building"):
    print("Building: Exited ", get_parent().name)
    GameManager.set_tile_occupation(null)


func run_state_process(delta):
  if current_state == States.Active:
    if $ActiveState.has_method("process"):
      $ActiveState.process(delta)
  if current_state == States.Spent:
    if $SpentState.has_method("process"):
      $SpentState.process(delta)
  if current_state == States.None:
    pass


func run_state_physics_process(delta):
  if current_state == States.Active:
    if $ActiveState.has_method("physics_process"):
      $ActiveState.physics_process(delta)
  if current_state == States.Spent:
    if $SpentState.has_method("physics_process"):
      $SpentState.physics_process(delta)
  if current_state == States.None:
    pass


func change_state(state):
  if state == States.Active:
    $ActiveState.enter_state(self)
    current_state = state
  if state == States.Spent:
    $SpentState.enter_state(self)
    current_state = state
  if state == States.None:
    current_state = state


func deliver_resource(gatherer: Node2D) -> int:
  var delivered_amount : int = 0
  if resource_amount >= gatherer.max_capacity:
    delivered_amount = gatherer.max_capacity
    resource_amount -= delivered_amount
  else:
    delivered_amount = resource_amount
    resource_amount = 0
  return delivered_amount

