extends TextureButton

export var scene_path: String = ""
export var building_name: String = ""


func _ready():
  StorageManager.connect("store_changes_done", self, "on_resources_changed")
  connect("button_up", self, "on_button_up")
  on_resources_changed()


func on_button_up():
  var building: PackedScene = load(scene_path)
  var instance: Node = building.instance()
  instance.assigned_priority = GameManager.global_priority
  get_node("/root/Map/Buildings").add_child(instance)
  instance.global_position = get_global_mouse_position()
  GameManager.emit_signal("new_building_selected", instance)


func on_resources_changed():
  var cost = Constants.BuildingCost[building_name]
  disabled = should_button_be_disabled(cost)


func should_button_be_disabled(cost):
  for resource_name in cost:
    var global_stored_amount = StorageManager.get_total_stored_amount(resource_name)
    if global_stored_amount < cost[resource_name]:
      return true
  return false
