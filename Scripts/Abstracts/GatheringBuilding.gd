extends "res://Scripts/Abstracts/Building.gd"

export var building_scene: String = ""
export var building_title: String = ""
export var floating_text_scene: PackedScene = null
export var floating_text_icon: Texture = null
export var text_offset: Vector2 = Vector2(0, -10)
export var max_capacity: int = 50
export var max_workers: int = 3
export var worker_type = "None"
export var relocation_priority: float = 1.0
export var relocation_weight: float = 1.0
export var resource_type = "None"

var detection_area: Area2D = null
var starting_construction_amount: int = 0
var capacity: int = 0
var reserved_storage: int = 0
var has_storage_available: bool = true
var relocation_amount: int = 0
var nearby_resources: Array = Array()
var workers: Array = Array()

var is_relocating: bool = false

var starting_state = States.Placing

func _enter_tree() -> void:
  loading_area = $LoadingArea
  detection_area = $DetectionArea
  animation_player = $AnimationPlayer
  sprite = $Sprite
  change_state(starting_state)


func _ready():
  $InputBuildingPanel/Grid/Active/PlusWorker.connect("button_down", self, "on_add_worker_pressed")
  $InputBuildingPanel/Grid/Active/MinusWorker.connect("button_down", self, "on_remove_worker_pressed")
  $InputBuildingPanel/Grid/Destroy/Button.connect("button_down", self, "on_destroy_pressed")
  $InputBuildingPanel/Grid/Construction/ProgressBar.max_value = construction_remaining
  $InputBuildingPanel.starting_construction_amount = construction_remaining
  $InputBuildingPanel.title = building_title
  $InputBuildingPanel.worker_type = worker_type
  $InputBuildingPanel.max_workers = max_workers
  $InputBuildingPanel.resource_type = resource_type
  $InputBuildingPanel.max_capacity = max_capacity


func _process(delta):
  $InputBuildingPanel.update_panel_labels(capacity, workers.size(), construction_remaining)
  var avaialble_capacity = max_capacity - capacity
  if avaialble_capacity > 0:
    has_storage_available = true
  else:
    has_storage_available = false
  run_state_process(delta)


func _physics_process(delta):
  run_state_physics_process(delta)


func disconnect_builders():
  .disconnect_builders()
  loading_area.connect("body_entered", self, "deliver_resource")


func deliver_resource(body : Node2D):
  if body.is_in_group(worker_type):
    var worker = body
    var worker_is_carrying_resource = worker.current_state == worker.States.Carrying
    if worker_is_carrying_resource:
      var resource = worker.deliver_resources()
      if (capacity + resource.amount) <= max_capacity:
        capacity += resource.amount
        spawn_text("" + str(resource.amount))
      else:
        capacity = max_capacity
        spawn_text(str(max_capacity))
  if body.is_in_group(Constants.Jobs.Hauler) and relocation_amount > 0:
    var hauler = body
    if capacity >= Constants.HAULER_CAPACITY:
      hauler.capacity = Constants.HAULER_CAPACITY
      hauler.item_type = resource_type
      capacity -= Constants.HAULER_CAPACITY
      if relocation_amount - Constants.HAULER_CAPACITY <= 0:
        relocation_amount = 0
      else:
        relocation_amount -= Constants.HAULER_CAPACITY
      is_relocating = false
    else:
      hauler.capacity = capacity
      hauler.item_type = resource_type
      capacity = 0
      relocation_amount = 0


func request_worker():
  var worker = VillagerManager.request_new_villager(worker_type)
  if worker != null:
    worker.set_building(self)
    workers.append(worker)


func get_nearest_resources():
  var shortest_distance : float = INF
  var selected_resource
  for resource in nearby_resources:
    if resource == null:
      nearby_resources.erase(resource)
    else:
      var distance = global_position.distance_to(resource.global_position)
      if distance < shortest_distance:
        shortest_distance = distance
        selected_resource = resource
  return selected_resource


func reserve_resource(resource_name : String, amount : int):
  pass


func on_self_click():
  $InputBuildingPanel.visible = not $InputBuildingPanel.visible


func on_add_worker_pressed():
  if workers.size() < max_workers:
    var worker = VillagerManager.request_new_villager(worker_type)
    if worker != null:
      worker.set_building(self)
      workers.append(worker)


func on_remove_worker_pressed():
  if workers.size() > 0:
    var worker = workers[0]
    workers.erase(worker)
    worker.drop_inventory_and_revert_to_villager()


func relocate_resources():
  var remaining_amount = capacity - relocation_amount
  if remaining_amount > 0:
    if remaining_amount >= Constants.HAULER_CAPACITY:
      do_relocation(resource_type, Constants.HAULER_CAPACITY)
    elif remaining_amount < Constants.HAULER_CAPACITY:
      do_relocation(resource_type, remaining_amount)


func do_relocation(resource: String, amount: int):
  var warehouse = StorageManager.request_storage(resource, amount)
  if warehouse != null:
    var result = warehouse.add_incoming_resource(resource, amount)
    if result:
      relocation_amount += Constants.HAULER_CAPACITY
      TaskManager.create_haul(relocation_priority, relocation_weight, resource, amount, warehouse, self)
      reserve_resource(resource, amount)


func spawn_text(text : String):
  var floating_text = floating_text_scene.instance()
  floating_text.global_position = Vector2(position.x + text_offset.x, position.y + text_offset.y)
  floating_text.z_index = 100
  floating_text.get_node("TextureRect").texture = floating_text_icon
  floating_text.set_text("+" + text)
  get_tree().root.get_node("Map/Effects").add_child(floating_text)


func on_destroy_pressed():
  var dropped_resources: Dictionary = StorageManager.make_populated_storage_dict()
  dropped_resources[resource_type] += 5
  if capacity > 0:
    dropped_resources[resource_type] += capacity
  destroy_and_drop_resources(dropped_resources)


func save_data(file):
  var nearby_resource_id_list: Array = []
  for resource in nearby_resources:
    nearby_resource_id_list.append(resource.data_id)
  var state: int = current_state
  var data: Dictionary = {
    "data_id": data_id,
    "building_title": building_title,
    "scene": building_scene,
    "type": "building",
    "global_position": {
      "x": global_position.x,
      "y": global_position.y
    },
    "state": state,
    "is_pre_built": is_pre_built,
    "construction_remaining": construction_remaining,
    "required_resources": required_resources,
    "finished_construction": finished_construction,
    "capacity": capacity,
    "reserved_storage": reserved_storage,
    "relocation_amount": relocation_amount,
    "has_storage_available": has_storage_available,
    "nearby_resources": nearby_resource_id_list,
    "worker_type": worker_type,
    "resource_type": resource_type,
    "max_workers": max_workers
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  building_title = data.building_title
  is_loaded = true
  is_pre_built = data.is_pre_built
  starting_state = data.state
  global_position = Vector2(data.global_position.x, data.global_position.y)
  construction_remaining = data.construction_remaining
  required_resources = data.required_resources
  finished_construction = data.finished_construction
  capacity = data.capacity
  reserved_storage = data.reserved_storage
  relocation_amount = data.relocation_amount
  has_storage_available = data.has_storage_available
  worker_type = data.worker_type
  resource_type = data.resource_type
  max_workers = data.max_workers
  for id in data.nearby_resources:
    var resource = GameManager.get_node_by_id(id)
    nearby_resources.append(resource)
