extends KinematicBody2D

export var max_idle_movement_timer: float = 5
export var min_idle_movement_timer: float = 2
export var speed: float = 50.0

var data_id: String = ""
var is_loaded: bool = false
var current_state = States.None
var facing = Direction.Right

var local_idle_point: Vector2 = Constants.IDLE_POINT
var vector: Vector2 = Vector2.ZERO
var target_position: Vector2 = Vector2.ZERO
var idle_movement_timer: float = 0
var velocity: Vector2 = Vector2.ZERO

enum States {
    None = 0,
    Idle = 1,
    Loading = 2,
    Carrying = 3,
    Working = 4,
}

enum Direction {
    Idle,
    Left,
    Right
}

func _enter_tree():
    if data_id == "":
        data_id = String(get_instance_id())
    GameManager.connect("save_buildings_called", self, "save_data")


func update_facing_direction():
    var direction = (target_position - global_position).normalized()
    if direction.x < 0.0:
        $Sprite.flip_h = true
        facing = Direction.Left
    if direction.x >= 0.0:
        $Sprite.flip_h = false
        facing = Direction.Right


func idle_move(delta: float, radius: float):
    idle_movement_tick(delta)
    if idle_movement_timer <= 0.0:
        get_new_target(radius)
    if (target_position - position).length() > 1.0:
        velocity = move_and_slide(vector * speed)


func get_new_target(radius: float):
    var rand_x = rand_range(local_idle_point.x - radius, local_idle_point.x + radius)
    var rand_y = rand_range(local_idle_point.y - radius, local_idle_point.y + radius)
    target_position = Vector2(rand_x, rand_y)
    vector = (target_position - position).normalized()
    idle_movement_timer = rand_range(min_idle_movement_timer, max_idle_movement_timer)


func idle_movement_tick(delta: float):
    if idle_movement_timer > 0:
        idle_movement_timer -= delta


func change_state(state):
    if state == States.Idle and $IdleState != null and state != current_state:
        $IdleState.enter_state(self)
        current_state = state
    if state == States.Loading and $LoadingState != null and state != current_state:
        $LoadingState.enter_state(self)
        current_state = state
    if state == States.Carrying and $CarryingState != null and state != current_state:
        $CarryingState.enter_state(self)
        current_state = state
    if state == States.Working and $WorkingState != null and state != current_state:
        $WorkingState.enter_state(self)
        current_state = state
    if state == States.None and state != current_state:
        current_state = state


func run_state_process(delta):
    if current_state == States.Idle and $IdleState != null:
        if $IdleState.has_method("process"):
            $IdleState.process(delta)
    if current_state == States.Loading and $LoadingState != null:
        if $LoadingState.has_method("process"):
            $LoadingState.process(delta)
    if current_state == States.Carrying and $CarryingState != null:
        if $CarryingState.has_method("process"):
            $CarryingState.process(delta)
    if current_state == States.Working and $WorkingState != null:
        if $WorkingState.has_method("process"):
            $WorkingState.process(delta)
    if current_state == States.None:
        pass


func run_state_physics_process(delta):
    if current_state == States.Idle and $IdleState != null:
        if $IdleState.has_method("physics_process"):
            $IdleState.physics_process(delta)
    if current_state == States.Loading and $LoadingState != null:
        if $LoadingState.has_method("physics_process"):
            $LoadingState.physics_process(delta)
    if current_state == States.Carrying and $CarryingState != null:
        if $CarryingState.has_method("physics_process"):
            $CarryingState.physics_process(delta)
    if current_state == States.Working and $WorkingState != null:
        if $WorkingState.has_method("physics_process"):
            $WorkingState.physics_process(delta)
    if current_state == States.None:
        pass
