extends "res://Scripts/Abstracts/Building.gd"

export var max_capacity : int = 0

export var storage: Dictionary = Dictionary()
var reserved_resources: Dictionary = Dictionary()
var incoming_resources: Dictionary = Dictionary()


func add_to_storage(resource_name : String, amount : int):
	if get_total_usage() < max_capacity:
		var overflow: int = 0
		if not storage.has(resource_name):
			storage[resource_name] = 0
		if storage[resource_name] + amount < max_capacity:
			storage[resource_name] += amount
			StorageManager.update_storage(resource_name, amount)
		else:
			overflow = (storage[resource_name] + amount) - max_capacity
			storage[resource_name] += amount - overflow
			StorageManager.update_storage(resource_name, amount - overflow)
		return { "added": true, "overflow": overflow }
	else:
		return { "added": false, "overflow": 0 }


func remove_from_storage(resource_name : String, amount : int):
	if get_total_usage() > 0:
		if storage[resource_name] >= amount:
			storage[resource_name] -= amount
			reserved_resources[resource_name] -= amount
			StorageManager.update_storage(resource_name, -amount)
			return { "removed": true, "remaining": 0 }
		else:
			var remaining = storage[resource_name]
			storage[resource_name] = 0
			StorageManager.update_storage(resource_name, -remaining)
			return { "removed": true, "remaining": remaining }
	else:
		return { "removed": false, "remaining": 0 }


func get_total_usage() -> int:
	var usage: int = 0
	for value in storage.values():
		usage += value
	return usage


func get_incoming_total() -> int:
	var incoming: int = 0
	for value in incoming_resources.values():
		incoming += value
	return incoming


func reserve_resource(resource_name : String, amount : int) -> bool:
	if not storage.has(resource_name):
		storage[resource_name] = 0
	if storage[resource_name] >= amount:
		if not reserved_resources.has(resource_name):
			reserved_resources[resource_name] = 0
		reserved_resources[resource_name] += amount
		return true
	else:
		return false


func add_incoming_resource(resource_name : String, amount : int) -> bool:
	if not incoming_resources.has(resource_name):
		incoming_resources[resource_name] = 0
	if (get_total_usage() + amount) <= max_capacity:
		incoming_resources[resource_name] += amount
		return true
	else:
		return false
