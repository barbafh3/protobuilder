extends TileMap

var current_tile

func _process(delta):
  var mouse_pos = get_global_mouse_position()
  var tile_pos = map_to_world(world_to_map(mouse_pos))
  var half_tile = Constants.TILE_SIZE / 2
  current_tile = Vector2(tile_pos.x + half_tile, tile_pos.y + half_tile)
  GameManager.set_current_mouse_tile(current_tile)
