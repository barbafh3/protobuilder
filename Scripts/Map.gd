extends Node2D

var hauler : PackedScene = preload("res://Scenes/Characters/Hauler.tscn")
var builder : PackedScene = preload("res://Scenes/Characters/Builder.tscn")


func _process(delta: float) -> void:
	check_for_villager_requests()


func check_for_villager_requests():
	if Input.is_action_just_released("hauler"):
		var _hauler = VillagerManager.request_new_villager(Constants.Jobs.Hauler)
	if Input.is_action_just_released("builder"):
		var _builder = VillagerManager.request_new_villager(Constants.Jobs.Builder)


