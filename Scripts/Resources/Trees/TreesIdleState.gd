extends Node2D

var state_name: String = ""
var trees = null

func enter_state(parent):
  state_name = "active"
  trees = parent
  var is_connected_to_entry = trees.is_connected("body_entered", self, "on_body_entered")
  var is_connected_to_exit = trees.is_connected("body_exited", self, "on_body_exited")
  if not is_connected_to_entry and not is_connected_to_exit:
    trees.connect("body_entered", self, "on_body_entered")
    trees.connect("body_exited", self, "on_body_exited")


func process(delta):
  if trees.resource_amount <= 0:
    exit_state()


func on_body_entered(body : Node2D):
  if body.is_in_group("Woodcutter"):
    if body.current_trees == trees:
      body.is_inside_trees = true


func on_body_exited(body : Node2D):
  if body.is_in_group("Woodcutter"):
    if body.current_trees == trees:
      body.is_inside_trees = false


func exit_state():
  trees.emit_signal("resource_spent", trees)
  trees.queue_free()
