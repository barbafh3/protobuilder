extends "res://Scripts/Abstracts/Storage.gd"

var has_requested_haul: bool = false


func _ready():
	$LoadingArea.connect("body_entered", self, "handle_incoming_villager")


func _process(delta):
	check_if_empty()
	if get_total_usage() > 0 and not has_requested_haul:
		request_haul()


func handle_incoming_villager(body):
	if body.is_in_group("Hauler"):
		var hauler = body
		print("Crate: Self = ", self, " - Hauler origin = ", hauler.resource_origin)
		if hauler.current_state == hauler.States.Loading and hauler.resource_origin == self:
			print("Crate: hauler on right place")
			var result = remove_from_storage(hauler.item_type, hauler.amount_requested)
			if result.removed:
				if result.remaining == 0:
					hauler.capacity = hauler.amount_requested
				else:
					var removed_amount = hauler.amount_requested - result.remaining
					hauler.capacity = removed_amount


func remove_from_storage(resource_name : String, amount : int):
	if get_total_usage() > 0:
		if storage[resource_name] >= amount:
			storage[resource_name] -= amount
			StorageManager.update_storage(resource_name, -amount)
			return { "removed": true, "remaining": 0 }
		else:
			var remaining = storage[resource_name]
			storage[resource_name] = 0
			StorageManager.update_storage(resource_name, -remaining)
			return { "removed": true, "remaining": remaining }


func get_total_usage() -> int:
	var usage: int = 0
	for value in storage.values():
		usage += value
	return usage


func check_if_empty():
	if get_total_usage() <= 0:
		StorageManager.unregister_storage(self)
		queue_free()


func request_haul():
	for key in storage.keys():
		var warehouse = StorageManager.request_storage(key, storage[key])
		TaskManager.create_haul(1, 1, key, storage[key], warehouse, self)
	has_requested_haul = true
