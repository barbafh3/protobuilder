extends "res://Scripts/Abstracts/GameResource.gd"


func _ready() -> void:
	change_state(States.Active)


func _process(delta):
	run_state_process(delta)


func _physics_process(delta):
	run_state_physics_process(delta)


func save_data(file):
	var state: int = current_state
	var data = {
		"data_id": data_id,
		"scene": "res://Scenes/Resources/Stones.tscn",
		"type": "resource",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y,
		},
		"resource_amount": resource_amount,
		"state": state,
	}
	file.store_line(to_json(data))


func load_data(data):
	data_id = data.data_id
	global_position = Vector2(data.global_position.x, data.global_position.y)
	resource_amount = data.resource_amount
	change_state(data.state)