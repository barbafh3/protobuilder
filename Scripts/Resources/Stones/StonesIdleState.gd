extends Node2D

var state_name: String = ""
var stones = null

func enter_state(parent):
	state_name = "active"
	stones = parent
	if not stones.is_connected("body_entered", self, "on_body_entered"):
		stones.connect("body_entered", self, "on_body_entered")
	if not stones.is_connected("body_exited", self, "on_body_exited"):
		stones.connect("body_exited", self, "on_body_exited")


func process(delta):
	if stones.resource_amount <= 0:
		exit_state()


func on_body_entered(body : Node2D):
	if body.is_in_group("Stonecutter"):
		if body.current_stones == stones:
			body.is_inside_stones = true


func on_body_exited(body : Node2D):
	if body.is_in_group("Stonecutter"):
		if body.current_stones == stones:
			body.is_inside_stones = false


func exit_state():
	stones.emit_signal("resource_spent", stones)
	print("StonesIdle: No resources, destroying...")
	stones.queue_free()
