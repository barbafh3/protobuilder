extends Node2D

var state_name: String
var controller = null
var _new_position: Vector2
var _position: Vector2
var sprite: Sprite

var _ready: bool = false


func enter_state(parent):
  state_name = "placing"
  controller = parent
  sprite = controller.get_node("Sprite")
  GameManager.connect("new_building_selected", self, "on_new_building_selected")
  if controller.is_pre_built:
    print("BuildingPlacing: New state = ", controller.States.Active)
    exit_state(controller.States.Active)
  else:
    controller.z_index = 50
    sprite.self_modulate = Color.green
    sprite.self_modulate.a = 0.5
    if controller.animation_player != null:
      controller.animation_player.play("Finished")
    controller.global_position = GameManager.current_mouse_tile


func process(delta : float):
  if controller == null:
    controller = get_parent()
  if not controller.is_pre_built:
    controller.global_position = GameManager.current_mouse_tile
  if not (GameManager.tile_building == null) and not (GameManager.tile_building == controller):
    sprite.self_modulate = Color.red
    sprite.self_modulate.a = 0.5
  else:
    sprite.self_modulate = Color.green
    sprite.self_modulate.a = 0.5


func physics_process(delta : float):
  if not controller.is_pre_built:
    if controller == null:
      controller = get_parent()
    if not _ready:
      _ready = true


func _unhandled_input(event):
  if _ready and controller.current_state == controller.States.Placing:
    var right_mouse_was_clicked = event is InputEventMouseButton and event.button_index == BUTTON_RIGHT and event.is_pressed()
    var left_mouse_was_clicked = event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.is_pressed()
    if right_mouse_was_clicked:
      controller.queue_free()
      queue_free()
    if left_mouse_was_clicked:
      if GameManager.tile_building == null:
        exit_state(controller.States.Loading)


func on_new_building_selected(building):
  if building != controller:
    controller.queue_free()
    queue_free()


func exit_state(state):
  GameManager.disconnect("new_building_selected", self, "on_new_building_selected")
  controller.change_state(state)
