extends "res://Scripts/Abstracts/Building.gd"

export var floating_text_scene: PackedScene = null
export var floating_text_icon: Texture = null
export var text_offset: Vector2 = Vector2.ZERO
export var max_capacity: int = 0
export var max_stonecutters: int = 0
export var relocation_priority: float = 1.0
export var relocation_weight: float = 1.0

var detection_area: Area2D = null
var starting_construction_amount: int = 0
var capacity: int = 0
var reserved_storage: int = 0
var has_storage_available: bool = true
var relocation_amount: int = 0
var nearby_stones: Array = Array()
var stonecutters: Array = Array()

var is_relocating: bool = false

var starting_state = States.Placing


func _enter_tree() -> void:
  loading_area = $LoadingArea
  detection_area = $DetectionArea
  animation_player = $AnimationPlayer
  sprite = $Sprite
  change_state(starting_state)


func _ready():
  $InputBuildingPanel/Grid/Destroy/Button.connect("button_down", self, "on_destroy_pressed")
  $InputBuildingPanel/Grid/Active/PlusWorker.connect("button_down", self, "on_add_stonecutter_pressed")
  $InputBuildingPanel/Grid/Active/MinusWorker.connect("button_down", self, "on_remove_stonecutter_pressed")
  $InputBuildingPanel/Grid/Construction/ProgressBar.max_value = construction_remaining
  $InputBuildingPanel.starting_construction_amount = construction_remaining
  $InputBuildingPanel.title = Constants.Buildings.Quarry
  $InputBuildingPanel.worker_type = Constants.Jobs.Stonecutter
  $InputBuildingPanel.max_workers = max_stonecutters
  $InputBuildingPanel.resource_type = Constants.Resources.Stone
  $InputBuildingPanel.max_capacity = max_capacity


func _process(delta):
  $InputBuildingPanel.update_panel_labels(capacity, stonecutters.size(), construction_remaining)
  var avaialble_capacity = max_capacity - capacity
  if avaialble_capacity > 0:
    has_storage_available = true
  else:
    has_storage_available = false
  run_state_process(delta)


func _physics_process(delta):
  run_state_physics_process(delta)


func disconnect_builders():
  .disconnect_builders()
  loading_area.connect("body_entered", self, "deliver_resource")


func deliver_resource(body : Node2D):
  if body.is_in_group(Constants.Jobs.Stonecutter):
    var stonecutter = body
    var stonecutter_is_carrying_stone = stonecutter.current_state == stonecutter.States.Carrying
    if stonecutter_is_carrying_stone:
      var resource = body.deliver_resources()
      if (capacity + resource.amount) <= max_capacity:
        capacity += resource.amount
        spawn_text("" + str(resource.amount))
      else:
        capacity = max_capacity
        spawn_text(str(max_capacity))
  if body.is_in_group(Constants.Jobs.Hauler) and relocation_amount > 0:
    var hauler = body
    if capacity >= Constants.HAULER_CAPACITY:
      hauler.capacity = Constants.HAULER_CAPACITY
      hauler.item_type = Constants.Resources.Stone
      capacity -= Constants.HAULER_CAPACITY
      if relocation_amount - Constants.HAULER_CAPACITY <= 0:
        relocation_amount = 0
      else:
        relocation_amount -= Constants.HAULER_CAPACITY
      is_relocating = false
    else:
      hauler.capacity = capacity
      hauler.item_type = Constants.Resources.Stone
      capacity = 0
      relocation_amount = 0


func request_stonecutter():
  var stonecutter = VillagerManager.request_new_villager(Constants.Jobs.Stonecutter)
  if stonecutter != null:
    stonecutter.set_building(self)
    stonecutters.append(stonecutter)


func get_nearest_stones():
  var shortest_distance : float = INF
  var selected_stones
  for stones in nearby_stones:
    if stones == null:
      nearby_stones.erase(stones)
    else:
      var distance = global_position.distance_to(stones.global_position)
      if distance < shortest_distance:
        shortest_distance = distance
        selected_stones = stones
  return selected_stones


func reserve_resource(resource_name : String, amount : int):
  pass


func on_self_click():
  $InputBuildingPanel.visible = not $InputBuildingPanel.visible


func on_add_stonecutter_pressed():
  if stonecutters.size() < max_stonecutters:
    var stonecutter = VillagerManager.request_new_villager(Constants.Jobs.Stonecutter)
    if stonecutter != null:
      stonecutter.set_building(self)
      stonecutters.append(stonecutter)


func on_remove_stonecutter_pressed():
  if stonecutters.size() > 0:
    var stonecutter = stonecutters[0]
    stonecutters.erase(stonecutter)
    stonecutter.drop_inventory_and_revert_to_villager()


func relocate_resources():
  var remaining_amount = capacity - relocation_amount
  if remaining_amount > 0:
    if remaining_amount >= Constants.HAULER_CAPACITY:
      do_relocation(Constants.Resources.Stone, Constants.HAULER_CAPACITY)
    elif remaining_amount < Constants.HAULER_CAPACITY:
      do_relocation(Constants.Resources.Stone, remaining_amount)


func do_relocation(resource: String, amount: int):
  var warehouse = StorageManager.request_storage(resource, amount)
  if warehouse != null:
    var result = warehouse.add_incoming_resource(resource, amount)
    if result:
      relocation_amount += Constants.HAULER_CAPACITY
      TaskManager.create_haul(relocation_priority, relocation_weight, resource, amount, warehouse, self)
      reserve_resource(resource, amount)


func spawn_text(text : String):
  var floating_text = floating_text_scene.instance()
  floating_text.global_position = Vector2(position.x + text_offset.x, position.y + text_offset.y)
  floating_text.z_index = 100
  floating_text.get_node("TextureRect").texture = floating_text_icon
  floating_text.set_text("+" + text)
  get_tree().root.get_node("Map/Effects").add_child(floating_text)


func save_data(file):
  var nearby_stone_id_list: Array = []
  for stone in nearby_stones:
    nearby_stone_id_list.append(stone.data_id)
  var state: int = current_state
  var data: Dictionary = {
    "data_id": data_id,
    "scene": "res://Scenes/Buildings/Quarry.tscn",
    "type": "building",
    "global_position": {
      "x": global_position.x,
      "y": global_position.y
    },
    "state": state,
    "is_pre_built": is_pre_built,
    "construction_remaining": construction_remaining,
    "required_resources": required_resources,
    "finished_construction": finished_construction,
    "capacity": capacity,
    "reserved_storage": reserved_storage,
    "relocation_amount": relocation_amount,
    "has_storage_available": has_storage_available,
    "nearby_stones": nearby_stone_id_list,
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  is_loaded = true
  is_pre_built = data.is_pre_built
  starting_state = data.state
  global_position = Vector2(data.global_position.x, data.global_position.y)
  construction_remaining = data.construction_remaining
  required_resources = data.required_resources
  finished_construction = data.finished_construction
  capacity = data.capacity
  reserved_storage = data.reserved_storage
  relocation_amount = data.relocation_amount
  has_storage_available = data.has_storage_available
  for id in data.nearby_stones:
    var stone = GameManager.get_node_by_id(id)
    nearby_stones.append(stone)


func on_destroy_pressed():
  var dropped_resources: Dictionary = StorageManager.make_populated_storage_dict()
  dropped_resources[Constants.Resources.Wood] += 5
  if capacity > 0:
    dropped_resources[Constants.Resources.Stone] += capacity
  destroy_and_drop_resources(dropped_resources)
