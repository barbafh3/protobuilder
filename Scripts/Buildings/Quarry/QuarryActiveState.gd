extends Node2D

var state_name: String
var quarry = null


func enter_state(parent):
  state_name = "active"
  quarry = parent
  if not quarry.is_pre_built:
    quarry.disconnect_builders()
  quarry.detection_area.monitorable = true
  quarry.animation_player.play("Finished")


func process(delta : float):
  if quarry == null:
    quarry = get_parent()
  check_for_resources()
  var relocation_space = quarry.max_capacity - quarry.relocation_amount
  var can_relocate_stones: bool = quarry.capacity > 0 and relocation_space >= quarry.capacity
  if can_relocate_stones:
    quarry.relocate_resources()
    quarry.is_relocating = true


func check_for_resources():
  var areas = quarry.detection_area.get_overlapping_areas()
  if areas.size() > 0:
    for area in areas:
      if area.is_in_group("Stones") and not quarry.nearby_resources.has(area):
        area.connect("resource_spent", self, "on_resources_destroyed")
        quarry.nearby_resources.append(area)


func on_resources_destroyed(resources):
  quarry.nearby_resources.erase(resources)

