extends Area2D

var parent = null


func _ready():
	parent = get_parent()


func _input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton \
			and event.button_index == BUTTON_LEFT \
			and event.is_pressed():
		if parent.current_state == parent.States.Active or parent.current_state == parent.States.Disabled:
			self.on_click()
		elif parent.placing_ready:
			self.on_click()



func on_click():
	if get_parent().current_state != get_parent().States.Placing:
		parent.on_self_click()
