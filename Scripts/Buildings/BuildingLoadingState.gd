extends Node2D

var state_name: String
var controller = null
var haul_script = preload("res://Scripts/Tasks/Haul.gd")
var has_created_haul = false
var sprite: Sprite

func enter_state(parent):
  state_name = "loading"
  controller = parent
  controller.z_index = 1
  sprite = controller.get_node("Sprite")
  sprite.self_modulate = Color.white
  sprite.self_modulate.a = 1.0
  controller.animation_player.play("Building")
  controller.connect_to_area()


func process(delta : float):
  if controller == null:
    controller = get_parent()
  if are_resources_loaded():
    exit_state()
  if not controller.is_loaded  and not has_created_haul:
    create_haul_tasks()
  if not controller.placing_ready:
    controller.placing_ready = true


func are_resources_loaded() -> bool:
  var result: bool = false
  for key in controller.required_resources:
    if controller.required_resources[key] <= 0:
      result = true
    else:
      result = false
  return result


func create_haul_tasks():
  for key in controller.required_resources:
    if controller.required_resources[key] > 0:
      TaskManager.create_haul(
        controller.assigned_priority,
        1,
        key,
        controller.required_resources[key],
        controller)
      has_created_haul = true


func exit_state():
  if controller.is_loaded:
    controller.is_loaded = false
  controller.change_state(controller.States.Construction)
