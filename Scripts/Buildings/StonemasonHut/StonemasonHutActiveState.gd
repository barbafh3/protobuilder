extends Node2D

var state_name: String
var stonehut = null


func enter_state(parent):
  state_name = "active"
  stonehut = parent
  stonehut.disconnect_builders()
  stonehut.loading_area.connect("body_entered", self, "on_body_entered")
  stonehut.loading_area.connect("body_exited", self, "on_body_exited")
  stonehut.animation_player.current_animation = "Finished"


func process(delta : float):
  if stonehut == null:
    stonehut = get_parent()
  var total_input = stonehut.input_storage + stonehut.reserved_input_storage
  var stonehut_requires_more_stone: bool = stonehut.max_capacity - total_input >= Constants.HAULER_CAPACITY
  if stonehut_requires_more_stone and stonehut.input_storage < stonehut.max_capacity:
    request_resource()
  var remaining_planks: int = stonehut.output_storage - stonehut.relocation_amount
  if remaining_planks >= Constants.HAULER_CAPACITY:
    relocate_production(remaining_planks)


func on_body_entered(body : Node2D):
  if body.is_in_group(Constants.Jobs.Carpenter):
    if body.assigned_building == stonehut:
      body.is_inside_building = true


func on_body_exited(body : Node2D):
  if body.is_in_group(Constants.Jobs.Carpenter):
    if body.assigned_building == stonehut:
      body.is_inside_building = false


func request_resource():
  var request_amount: int = 0
  if (stonehut.max_capacity - stonehut.input_storage) >= Constants.HAULER_CAPACITY:
    request_amount = stonehut.max_capacity - stonehut.input_storage
  else:
    request_amount = Constants.HAULER_CAPACITY
  var available_storage = stonehut.remaining_available_input_storage()
  var canRequestMoreResources = available_storage > 0
  var storage_has_enough_stone = StorageManager.get_total_stored_amount(Constants.Resources.Stone) >= request_amount
  if request_amount > 0 and canRequestMoreResources and storage_has_enough_stone:
    if available_storage >= request_amount:
      TaskManager.create_haul(1, 1, Constants.Resources.Stone, request_amount, stonehut)
      stonehut.reserved_input_storage += request_amount
    else:
      TaskManager.create_haul(1, 1, Constants.Resources.Stone, available_storage, stonehut)
      stonehut.reserved_input_storage += available_storage

  
func relocate_production(remaining_amount: int):
  if remaining_amount >= Constants.HAULER_CAPACITY:
    request_storage_and_relocate(Constants.Resources.StoneBrick,  Constants.HAULER_CAPACITY)
  else:
    request_storage_and_relocate(Constants.Resources.StoneBrick,  remaining_amount)


func request_storage_and_relocate(resource: String, amount: int):
  var warehouse = StorageManager.request_storage(resource, amount)
  if warehouse != null:
    var resource_added_to_storage = warehouse.add_incoming_resource(resource, amount)
    if resource_added_to_storage:
      stonehut.relocation_amount += amount
      TaskManager.create_haul(1, 1, resource, amount, warehouse, stonehut)

