extends Node2D

var state_name: String
var woodhut = null


func enter_state(parent):
  state_name = "WoodcuttersHutActive"
  woodhut = parent
  if not woodhut.is_pre_built:
    woodhut.disconnect_builders()
  woodhut.detection_area.monitorable = true
  woodhut.animation_player.play("Finished")


func process(delta : float):
  if woodhut == null:
    woodhut = get_parent()
  check_for_trees()
  var relocation_space = woodhut.max_capacity - woodhut.relocation_amount
  var can_relocate_wood: bool = woodhut.capacity > 0 and relocation_space >= woodhut.capacity
  if can_relocate_wood:
    woodhut.relocate_resources()
    woodhut.is_relocating = true


func check_for_trees():
  var areas = woodhut.detection_area.get_overlapping_areas()
  if areas.size() > 0:
    for area in areas:
      if area.is_in_group("Trees") and not woodhut.nearby_resources.has(area):
        area.connect("resource_spent", self, "on_trees_destroyed")
        woodhut.nearby_resources.append(area)


func on_trees_destroyed(trees):
  woodhut.nearby_resources.erase(trees)
