extends Node2D

var parent = null
var starting_construction_amount: int = 0
var title: String = ""
var input_resource_type: String = ""
var max_input_capacity: int = 0
var output_resource_type: String = ""
var max_output_capacity: int = 0
var worker_type: String = ""
var max_workers: int = 0

var active_height_set: bool = false


func _ready():
	parent = get_parent()
	GameManager.connect("mouse_clicked", self, "check_if_clicked")


func check_if_clicked(event):
	var event_local = make_input_local(event)
	if !Rect2($Grid.rect_position, Vector2($Grid.rect_size.x, $Grid.rect_size.y)).has_point(event_local.position):
		parent.get_node("ProductionBuildingPanel").visible = false



func update_panel_labels(input_capacity, output_capacity, workers, construction_remaining):
	if parent.current_state == parent.States.Active:
		$Grid/Construction.visible = false
		$Grid/Active.visible = true
		if not active_height_set:
			var total_height = 0
			for child in $Grid.get_children():
				if child.visible:
					total_height += child.rect_size.y
			$Grid.rect_size = Vector2($Grid.rect_size.x, total_height)
			active_height_set = true
	else:
		$Grid/Active.visible = false
		$Grid/Construction.visible = true
	$Grid/Title/Label.text = title
	$Grid/Active/Input.text = input_resource_type
	$Grid/Active/Output.text = output_resource_type
	$Grid/Active/Workers.text = worker_type
	$Grid/Active/InputValue.text = String(input_capacity) + "/" + String(max_input_capacity)
	$Grid/Active/OutputValue.text = String(output_capacity) + "/" + String(max_output_capacity)
	$Grid/Active/WorkersValue.text = String(workers) + "/" + String(max_workers)
	$Grid/Construction/ProgressBar.value = starting_construction_amount - construction_remaining
	if workers >= max_workers:
		$Grid/Active/PlusWorker.disabled = true
		$Grid/Active/PlusWorker/Label.modulate = Color(0.7, 0.7, 0.7, 1.0)
	else:
		$Grid/Active/PlusWorker.disabled = false
		$Grid/Active/PlusWorker/Label.modulate = Color(1, 1, 1, 1.0)
	if workers <= 0:
		$Grid/Active/MinusWorker.disabled = true
		$Grid/Active/MinusWorker/Label.modulate = Color(0.7, 0.7, 0.7, 1.0)
	else:
		$Grid/Active/MinusWorker.disabled = false
		$Grid/Active/MinusWorker/Label.modulate = Color(1, 1, 1, 1.0)
