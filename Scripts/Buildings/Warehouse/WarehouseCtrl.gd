extends "res://Scripts/Abstracts/Storage.gd"

export var floating_text_scene : PackedScene = null
export var text_offset : float = 0.0

var detection_area : Area2D = null
var has_storage_available : bool = true

var starting_state = States.None
var data_loaded: bool = false



func _enter_tree() -> void:
	loading_area = $LoadingArea
	animation_player = $AnimationPlayer
	sprite = $Sprite


func _ready() -> void:
	if is_pre_built:
		starting_state = States.Active
		change_state(States.Active)
	else:
		starting_state = States.Placing
		change_state(States.Placing)

	$StoragePanel/Grid/Destroy/Button.connect("button_down", self, "on_destroy_pressed")
	$StoragePanel.starting_construction_amount = construction_remaining
	$StoragePanel.title = Constants.Buildings.Warehouse
	$StoragePanel.max_capacity = max_capacity


func _process(delta):
	$StoragePanel.update_panel_labels(get_total_usage(), construction_remaining)
	if not data_loaded:
		change_state(starting_state)
		data_loaded = true
	run_state_process(delta)


func _physics_process(delta):
	run_state_physics_process(delta)


func on_self_click():
	$StoragePanel.visible = not $StoragePanel.visible


func handle_incoming_villager(body : Node2D):
	if body.is_in_group("Hauler"):
		var hauler = body
		if hauler.current_state == hauler.States.Loading and hauler.resource_origin == self:
			var result = remove_from_storage(hauler.item_type, hauler.amount_requested)
			if result.removed:
				if result.remaining == 0:
					hauler.capacity = hauler.amount_requested
				else:
					var removed_amount = hauler.amount_requested - result.remaining
					hauler.capacity = removed_amount
		if hauler.current_state == hauler.States.Carrying and hauler.resource_destination == self:
			var result = add_to_storage(hauler.item_type, hauler.amount_requested)
			if result.added:
				hauler.deliver_resources()


func check_for_present_haulers():
	var bodies = loading_area.get_overlapping_bodies()
	if bodies.size() > 0:
		for body in bodies:
			handle_incoming_villager(body)


func disconnect_builders():
	.disconnect_builders()
	if not loading_area.is_connected("body_entered", self, "handle_incoming_villager"):
		loading_area.connect("body_entered", self, "handle_incoming_villager")


func save_data(file):

	var data: Dictionary = {
		"data_id": data_id,
		"scene": "res://Scenes/Buildings/Warehouse.tscn",
		"type": "building",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y,
		},
		"state": current_state,
		"is_pre_built": is_pre_built,
		"construction_remaining": construction_remaining,
		"required_resources": required_resources,
		"finished_construction": finished_construction,
		"storage": storage,
		"reserved_resources": reserved_resources,
		"incoming_resources": incoming_resources,
		"has_storage_available": has_storage_available,
	}
	file.store_line(to_json(data))


func load_data(data):
	data_id = data.data_id
	is_loaded = true
	is_pre_built = data.is_pre_built
	starting_state = data.state
	global_position = Vector2(data.global_position.x, data.global_position.y)
	construction_remaining = data.construction_remaining
	required_resources = data.required_resources
	finished_construction = data.finished_construction
	storage = data.storage
	reserved_resources = data.reserved_resources
	incoming_resources = data.incoming_resources
	has_storage_available = data.has_storage_available


func on_destroy_pressed():
	var dropped_resources: Dictionary = StorageManager.make_populated_storage_dict()
	dropped_resources[Constants.Resources.Wood] += 5
	for resource in storage:
		if storage[resource] > 0:
			dropped_resources[resource] += storage[resource]
	StorageManager.unregister_storage(self)
	destroy_and_drop_resources(dropped_resources)

