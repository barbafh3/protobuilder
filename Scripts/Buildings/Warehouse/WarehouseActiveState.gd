extends Node2D

var state_name: String
var warehouse = null


func enter_state(parent):
	state_name = "WarehouseActive"
	warehouse = parent
	StorageManager.register_storage(warehouse)
	warehouse.animation_player.play("Finished")
	warehouse.disconnect_builders()
	# if warehouse.is_pre_built:
	# 	StorageManager.update_storage_from_dict(warehouse.storage)


func process(delta : float):
	if warehouse == null:
		warehouse = get_parent()
	if warehouse.loading_area.get_overlapping_bodies().size() > 0:
		warehouse.handle_incoming_villager(warehouse.loading_area.get_overlapping_bodies()[0])
