extends "res://Scripts/Abstracts/Building.gd"

export var floating_text_scene: PackedScene = null
export var floating_text_icon: Texture = null
export var text_offset: Vector2 = Vector2.ZERO
export var max_capacity: int = 0
export var max_carpenters: int = 0

var storage: Dictionary = { Constants.Resources.Plank: 0 }
var input_storage: int = 0
var output_storage: int = 0
var reserved_input_storage: int = 0
var reserved_storage: int = 0
var has_storage_available: int = true
var relocation_amount: int = 0
var carpenters: Array = Array()

var starting_state = States.Placing


func _enter_tree() -> void:
  animation_player = $AnimationPlayer
  loading_area = $LoadingArea
  sprite = $Sprite
  change_state(starting_state)


func _ready() -> void:
  $ProductionBuildingPanel/Grid/Destroy/Button.connect("button_down", self, "on_destroy_pressed")
  $ProductionBuildingPanel/Grid/Active/PlusWorker.connect("button_down", self, "on_add_carpenter_pressed")
  $ProductionBuildingPanel/Grid/Active/MinusWorker.connect("button_down", self, "on_remove_carpenter_pressed")
  $ProductionBuildingPanel/Grid/Construction/ProgressBar.max_value = construction_remaining
  $ProductionBuildingPanel.starting_construction_amount = construction_remaining
  $ProductionBuildingPanel.title = Constants.Buildings.Sawmill
  $ProductionBuildingPanel.worker_type = Constants.Jobs.Carpenter
  $ProductionBuildingPanel.max_workers = max_carpenters
  $ProductionBuildingPanel.input_resource_type = Constants.Resources.Wood
  $ProductionBuildingPanel.output_resource_type = Constants.Resources.Plank
  $ProductionBuildingPanel.max_input_capacity = max_capacity
  $ProductionBuildingPanel.max_output_capacity = max_capacity


func _process(delta):
  $ProductionBuildingPanel.update_panel_labels(input_storage, output_storage, carpenters.size(), construction_remaining)
  run_state_process(delta)


func _physics_process(delta):
  run_state_physics_process(delta)


func disconnect_builders():
  .disconnect_builders()
  loading_area.connect("body_entered", self, "deliver_resource")
  loading_area.connect("body_exited", self, "handle_haulers")


func deliver_resource(body : Node2D):
  if body.is_in_group(Constants.Jobs.Hauler):
    var hauler = body
    hauler.is_inside_building = true
    var hauler_is_loading = hauler.current_state == hauler.States.Loading
    var hauler_is_carrying = hauler.current_state == hauler.States.Carrying
    if hauler_is_loading and relocation_amount > 0:
      if output_storage >= Constants.HAULER_CAPACITY:
        hauler.capacity = Constants.HAULER_CAPACITY
        hauler.item_type = Constants.Resources.Plank
        output_storage -= Constants.HAULER_CAPACITY
        relocation_amount -= Constants.HAULER_CAPACITY
      else:
        hauler.capacity = output_storage
        hauler.item_type = Constants.Resources.Plank
        if relocation_amount - output_storage <= 0:
          relocation_amount = 0
        else:
          relocation_amount -= output_storage
        output_storage = 0
        relocation_amount = 0
    if hauler_is_carrying and hauler.resource_destination == self:
      input_storage += hauler.capacity
      if reserved_input_storage - hauler.capacity <= 0:
        reserved_input_storage = 0
      else:
        reserved_input_storage -= hauler.capacity
      hauler.capacity = 0


func handle_haulers(body):
  if body.is_in_group(Constants.Jobs.Hauler):
    body.is_inside_building = false


func request_carpenter():
  var carpenter = VillagerManager.request_new_villager(Constants.Jobs.Carpenter)
  if (carpenter != null):
    carpenter.set_building(self)


func on_self_click():
  $ProductionBuildingPanel.visible = not $ProductionBuildingPanel.visible


func on_add_carpenter_pressed():
  if carpenters.size() < max_carpenters:
    var carpenter = VillagerManager.request_new_villager(Constants.Jobs.Carpenter)
    if carpenter != null:
      carpenter.set_building(self)
      carpenters.append(carpenter)


func on_remove_carpenter_pressed():
  if carpenters.size() > 0:
    var carpenter = carpenters[0]
    carpenters.erase(carpenter)
    VillagerManager.revert_to_villager(carpenter)


func remaining_available_input_storage() -> float:
  return abs(max_capacity - (input_storage + reserved_input_storage))


func spawn_text(text : String):
  var floating_text = floating_text_scene.instance()
  floating_text.global_position = Vector2(position.x + text_offset.x, position.y + text_offset.y)
  floating_text.z_index = 100
  floating_text.get_node("TextureRect").texture = floating_text_icon
  floating_text.set_text("+" + text)
  get_tree().root.get_node("Map/Effects").add_child(floating_text)


func save_data(file):
  var data: Dictionary = {
    "data_id": data_id,
    "scene": "res://Scenes/Buildings/Sawmill.tscn",
    "type": "building",
    "global_position": {
      "x": global_position.x,
      "y": global_position.y,
    },
    "state": current_state,
    "is_pre_built": is_pre_built,
    "construction_remaining": construction_remaining,
    "required_resources": required_resources,
    "finished_construction": finished_construction,
    "input_storage": input_storage,
    "output_storage": output_storage,
    "reserved_input_storage": reserved_input_storage,
    "reserved_storage": reserved_storage,
    "has_storage_available": has_storage_available,
    "relocation_amount": relocation_amount,
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  is_loaded = true
  is_pre_built = data.is_pre_built
  starting_state = data.state
  global_position = Vector2(data.global_position.x, data.global_position.y)
  construction_remaining = data.construction_remaining
  required_resources = data.required_resources
  finished_construction = data.finished_construction
  input_storage = data.input_storage
  output_storage = data.output_storage
  reserved_input_storage = data.reserved_input_storage
  reserved_storage = data.reserved_storage
  has_storage_available = data.has_storage_available
  relocation_amount = data.relocation_amount


func on_destroy_pressed():
  var amount_to_drop: int = 5
  var dropped_resources = { Constants.Resources.Wood: amount_to_drop }
  if input_storage > 0:
    dropped_resources[Constants.Resources.Wood] += input_storage
  if output_storage > 0:
    dropped_resources[Constants.Resources.Plank] += output_storage
  destroy_and_drop_resources(dropped_resources)
