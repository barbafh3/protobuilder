extends Node2D

var state_name: String
var sawmill = null


func enter_state(parent):
  state_name = "active"
  sawmill = parent
  sawmill.disconnect_builders()
  sawmill.loading_area.connect("body_entered", self, "on_body_entered")
  sawmill.loading_area.connect("body_exited", self, "on_body_exited")
  sawmill.animation_player.current_animation = "Finished"


func process(delta : float):
  if sawmill == null:
    sawmill = get_parent()
  var total_input = sawmill.input_storage + sawmill.reserved_input_storage
  var sawmill_requires_more_wood: bool = sawmill.max_capacity - total_input >= Constants.HAULER_CAPACITY
  if sawmill_requires_more_wood and sawmill.input_storage < sawmill.max_capacity:
    request_resource()
  var remaining_planks: int = sawmill.output_storage - sawmill.relocation_amount
  if remaining_planks >= Constants.HAULER_CAPACITY:
    relocate_production(remaining_planks)


func on_body_entered(body : Node2D):
  if body.is_in_group(Constants.Jobs.Carpenter):
    if body.assigned_building == sawmill:
      body.is_inside_building = true


func on_body_exited(body : Node2D):
  if body.is_in_group(Constants.Jobs.Carpenter):
    if body.assigned_building == sawmill:
      body.is_inside_building = false


func request_resource():
  var request_amount: int = 0
  if (sawmill.max_capacity - sawmill.input_storage) >= Constants.HAULER_CAPACITY:
    request_amount = sawmill.max_capacity - sawmill.input_storage
  else:
    request_amount = Constants.HAULER_CAPACITY
  var available_storage = sawmill.remaining_available_input_storage()
  var canRequestMoreResources = available_storage > 0
  var storage_has_enough_wood = StorageManager.get_total_stored_amount(Constants.Resources.Wood) >= request_amount
  if request_amount > 0 and canRequestMoreResources and storage_has_enough_wood:
      print("SawmillActive: Available Storage = ", available_storage, " Input Storage = ", sawmill.input_storage, " Reserved sotrage = ", sawmill.reserved_input_storage)
      if available_storage >= request_amount:
        TaskManager.create_haul(1, 1, Constants.Resources.Wood, request_amount, sawmill)
        sawmill.reserved_input_storage += request_amount
      else:
        TaskManager.create_haul(1, 1, Constants.Resources.Wood, available_storage, sawmill)
        sawmill.reserved_input_storage += available_storage


func relocate_production(remaining_amount: int):
  if remaining_amount >= Constants.HAULER_CAPACITY:
    request_storage_and_relocate(Constants.Resources.Plank,  Constants.HAULER_CAPACITY)
  else:
    request_storage_and_relocate(Constants.Resources.Plank,  remaining_amount)


func request_storage_and_relocate(resource: String, amount: int):
  var warehouse = StorageManager.request_storage(resource, amount)
  if warehouse != null:
    var resource_added_to_storage = warehouse.add_incoming_resource(resource, amount)
    if resource_added_to_storage:
      sawmill.relocation_amount += amount
      TaskManager.create_haul(1, 1, resource, amount, warehouse, sawmill)

