extends Node2D

export var _villager_scene: PackedScene = preload("res://Scenes/Characters/Villager.tscn")

var state_name: String
var house = null


func enter_state(parent):
  state_name = "active"
  house = parent
  house.get_node("Sprite").self_modulate.a = 1
  house.disconnect_builders()
  house.animation_player.current_animation = "Finished"
  if not house.has_spawned_villager:
    VillagerManager.get_generic_villager(house.position)
    house.has_spawned_villager = true
