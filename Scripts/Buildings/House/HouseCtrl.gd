extends "res://Scripts/Abstracts/Building.gd"

var starting_state = States.Placing
var has_spawned_villager: bool = false


func _enter_tree() -> void:
  animation_player = $AnimationPlayer
  loading_area = $LoadingArea
  sprite = $Sprite
  change_state(starting_state)


func _ready() -> void:
  if data_id == "":
    data_id = String(get_instance_id())

  $SimpleBuildingPanel/Grid/Destroy/Button.connect("button_down", self, "on_destroy_pressed")
  $SimpleBuildingPanel.starting_construction_amount = construction_remaining
  $SimpleBuildingPanel.title = Constants.Buildings.House


func _process(delta):
  $SimpleBuildingPanel.update_panel_labels(construction_remaining)
  run_state_process(delta)


func _physics_process(delta):
  run_state_physics_process(delta)


func on_self_click():
  $SimpleBuildingPanel.visible = not $SimpleBuildingPanel.visible


func save_data(file):
  var data: Dictionary = {
    "data_id": data_id,
    "scene": "res://Scenes/Buildings/SmallHouse.tscn",
    "type": "building",
    "global_position": {
      "x": global_position.x,
      "y": global_position.y
    },
    "state": current_state,
    "is_pre_built": is_pre_built,
    "construction_remaining": construction_remaining,
    "required_resources": required_resources,
    "finished_construction": finished_construction,
    "has_spawned_villager": has_spawned_villager
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  is_loaded = true
  is_pre_built = data.is_pre_built
  starting_state = data.state
  global_position = Vector2(data.global_position.x, data.global_position.y)
  construction_remaining = data.construction_remaining
  required_resources = data.required_resources
  finished_construction = data.finished_construction
  has_spawned_villager = data.has_spawned_villager


func on_destroy_pressed():
  var amount_to_drop: int = 5
  var dropped_resources = { Constants.Resources.Wood: amount_to_drop }
  destroy_and_drop_resources(dropped_resources)


