extends Node2D

var parent = null
var starting_construction_amount: int = 0
var title: String = ""

var active_height_set: bool = false


func _ready():
	parent = get_parent()
	GameManager.connect("mouse_clicked", self, "check_if_clicked")


func check_if_clicked(event):
	var event_local = make_input_local(event)
	if !Rect2($Grid.rect_position, Vector2($Grid.rect_size.x, $Grid.rect_size.y)).has_point(event_local.position):
		parent.get_node("SimpleBuildingPanel").visible = false


func update_panel_labels(construction_remaining):
	if parent.current_state == parent.States.Active:
		$Grid/Construction.visible = false
		$Grid/InnerSeparator.visible = false
		if not active_height_set:
			var total_height = 0
			for child in $Grid.get_children():
				if child.visible:
					total_height += child.rect_size.y
			$Grid.rect_size = Vector2($Grid.rect_size.x, total_height)
			active_height_set = true
	else:
		$Grid/Construction.visible = true
	$Grid/Title/Label.text = title
	$Grid/Construction/ProgressBar.value = starting_construction_amount - construction_remaining
