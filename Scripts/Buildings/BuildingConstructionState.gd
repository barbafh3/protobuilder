extends Node2D

export var texture: Texture = null

var state_name: String
var controller = null
var build_script = preload("res://Scripts/Tasks/Build.gd")


func enter_state(parent):
	state_name = "construction"
	controller = parent
	controller.sprite.texture = texture
	controller.connect_to_builders()
	var build = build_script.new(controller.assigned_priority, 1, controller)
	TaskManager.register_task(build)


func process(delta : float):
	if controller == null:
		controller = get_parent()
	if controller.construction_remaining <= 0:
		exit_state()


func exit_state():
	controller.is_loaded = false
	controller.change_state(controller.States.Active)
