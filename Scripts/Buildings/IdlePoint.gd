extends Node2D

var data_id: String = ""


func _ready():
	if data_id == "":
		data_id = String(get_instance_id())
	GameManager.connect("save_buildings_called", self, "save_data")


func save_data(file):
	var data = {
		"data_id": data_id,
		"scene": "res://Scenes/World/IdlePoint.tscn",
		"type": "building",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y
		}
	}
	file.store_line(to_json(data))


func load_data(data):
	data_id = data.data_id
	global_position = Vector2(data.global_position.x, data.global_position.y)