extends "res://Scripts/Abstracts/Storage.gd"

export var item_type : String = ""

var starting_state = States.Active


func _ready() -> void:
	if data_id == "":
		data_id = String(get_instance_id())
	loading_area = $LoadingArea
	StorageManager.register_storage(self)
	for key in storage:
		StorageManager.update_storage(key, storage[key])
	loading_area.connect("body_entered", self, "handle_incoming_villager")
	GameManager.connect("save_buildings_called", self, "save_data")
	change_state(starting_state)


func _process(delta):
	run_state_process(delta)


func _physics_process(delta):
	run_state_physics_process(delta)


func handle_incoming_villager(body : Node2D):
	if body.is_in_group("Hauler"):
		var hauler = body
		# var hauler_current_state = hauler.machine.current_state.get_script().get_path()
		# var hauler_loading_state = hauler.states.loading.get_path()
		# var hauler_carrying_state = hauler.states.carrying.get_path()

		# var hauler_is_loading = hauler_current_state == hauler_loading_state
		var hauler_is_loading = hauler.current_state == hauler.States.Loading
		if hauler_is_loading and hauler.resource_origin == self:
			var result = remove_from_storage(hauler.item_type, hauler.amount_requested)
			if result.removed:
				if result.remaining == 0:
					hauler.capacity = hauler.amount_requested
				else:
					var removed_amount = (hauler.amount_requested - result.remaining)
					hauler.capacity = removed_amount
			else:
				hauler.loading_failed = true
		# var hauler_is_carrying = hauler_current_state == hauler_carrying_state
		var hauler_is_carrying = hauler.current_state == hauler.States.Carrying
		if hauler_is_carrying and hauler.resource_destination == self:
			var result = add_to_storage(hauler.item_type, hauler.amount_requested)
			if result.added:
				if result.overflow == 0:
					hauler.capacity = 0
				else:
					var delivered_amount = hauler.capacity - result.overflow
					hauler.capacity -= delivered_amount


func clear_stockpile():
	emit_signal("building_destroyed")
	StorageManager.unregister_storage(self)
	queue_free()


func save_data(file):
	var state: int = current_state
	var data: Dictionary = {
		"data_id": data_id,
		"scene": "res://Scenes/Buildings/Stockpile.tscn",
		"type": "building",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y,
		},
		"state": state,
		"construction_remaining": construction_remaining,
		"required_resources": required_resources,
		"finished_construction": finished_construction,
		"storage": storage,
		"reserved_resources": reserved_resources,
		"incoming_resources": incoming_resources,
	}
	file.store_line(to_json(data))


func load_data(data):
	starting_state = data.state
	storage = data.storage
	reserved_resources = data.reserved_resources
	incoming_resources = data.incoming_resources
	data_id = data.data_id
	global_position = Vector2(data.global_position.x, data.global_position.y)
	construction_remaining = data.construction_remaining
	required_resources = data.required_resources
	finished_construction = data.finished_construction
