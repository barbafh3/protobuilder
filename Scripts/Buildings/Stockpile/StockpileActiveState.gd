extends Node2D
# extends "res://Scripts/Abstracts/State.gd"

var state_name: String
var stockpile = null
var haul_script = preload("res://Scripts/Tasks/Haul.gd")

var _warehouse_found : bool = false


func enter_state(parent):
# func enter_state(machine):
# 	.enter_state(machine)
	state_name = "StockpileActive"
	stockpile = parent


func process(delta : float):
	if stockpile == null:
		stockpile = get_parent()
	if stockpile.get_total_usage() <= 0:
		stockpile.clear_stockpile()
	var overlapping_bodies = stockpile.loading_area.get_overlapping_bodies()
	for body in overlapping_bodies:
		stockpile.handle_incoming_villager(body)