extends Node2D

var state_name: String
var woodcutter = null


func enter_state(parent):
  state_name = "WoodcutterIdle"
  woodcutter = parent
  woodcutter.work_duration = woodcutter.base_work_duration
  print("WoodcutterIdle: Entering idle state...")
  woodcutter.get_new_target(16)
  woodcutter.get_node("AnimationPlayer").play("Idle")


func process(delta : float):
  if woodcutter == null:
    woodcutter = get_parent()
  if woodcutter.assigned_building != null:
    if woodcutter.assigned_building.has_storage_available:
      woodcutter.current_trees = woodcutter.assigned_building.get_nearest_resources()
      exit_state()
  woodcutter.idle_move(delta, Constants.INDOOR_IDLE_RADIUS)


func exit_state():
  woodcutter.change_state(woodcutter.States.Working)
