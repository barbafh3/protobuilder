extends Node2D

var target_position: Vector2
var state_name: String
var woodcutter = null
var has_reserved_resources: bool = false


func enter_state(parent):
    state_name = "WoodcutterWorking"
    woodcutter = parent
    get_new_target()
    has_reserved_resources = false
    if not woodcutter.current_trees.is_connected("resource_spent", self, "on_trees_destroyed"):
        woodcutter.current_trees.connect("resource_spent", self, "on_trees_destroyed")


func process(delta: float):
    if woodcutter == null:
        woodcutter = get_parent()
    var has_trees_selected: bool = woodcutter.current_trees != null
    if has_trees_selected:
        woodcutter.target_position = woodcutter.current_trees.position
        var trees_have_enough_resources: bool = woodcutter.current_trees.resource_amount >= woodcutter.max_capacity
        var resources_reserved: bool = woodcutter.current_trees.resource_amount <= woodcutter.current_trees.reserved_amount
        if trees_have_enough_resources:
            woodcutter.target_position = woodcutter.current_trees.global_position
			# TODO: Check next line for removal
            # var direction = (woodcutter.current_trees.global_position - woodcutter.global_position).normalized()
            if not resources_reserved and not has_reserved_resources:
                woodcutter.current_trees.reserved_amount += woodcutter.max_capacity
                has_reserved_resources = true
            if not woodcutter.is_inside_trees:
                woodcutter.get_node("AnimationPlayer").play("Idle")
                var vector: Vector2 = \
                    woodcutter.current_trees.global_position - woodcutter.global_position
                if vector.length() > 1.0:
                    woodcutter.move_and_slide(vector.normalized() * woodcutter.speed)
            else:
                if woodcutter.facing == woodcutter.Direction.Left:
                    woodcutter.get_node("AnimationPlayer").play("WorkingLeft")
                if woodcutter.facing == woodcutter.Direction.Right:
                    woodcutter.get_node("AnimationPlayer").play("WorkingRight")
                work_tick(delta)
                if woodcutter.work_duration <= 0:
                    if assigned_building_has_capacity():
                        woodcutter.capacity = woodcutter.current_trees.deliver_resource(woodcutter)
                        woodcutter.current_trees.reserved_amount -= woodcutter.max_capacity
                        exit_state(woodcutter.States.Carrying)
                    else:
                        exit_state(woodcutter.States.Idle)
        else:
            woodcutter.current_trees = null
            exit_state(woodcutter.States.Idle)
    else:
        exit_state(woodcutter.States.Idle)


func work_tick(delta: float):
    if woodcutter.work_duration > 0:
        woodcutter.work_duration -= delta


func assigned_building_has_capacity() -> bool:
    return woodcutter.assigned_building.capacity < woodcutter.assigned_building.max_capacity


func get_new_target():
    var rand_x = rand_range(woodcutter.current_trees.global_position.x - woodcutter.target_radius,
                            woodcutter.current_trees.global_position.x + woodcutter.target_radius)
    var rand_y = rand_range(woodcutter.current_trees.global_position.y - woodcutter.target_radius,
                            woodcutter.current_trees.global_position.y + woodcutter.target_radius)
    target_position = Vector2(rand_x, rand_y)


func on_trees_destroyed(trees):
    woodcutter.current_trees = null
    woodcutter.is_inside_trees = false


func exit_state(state):
    woodcutter.is_loaded = false
    woodcutter.change_state(state)
