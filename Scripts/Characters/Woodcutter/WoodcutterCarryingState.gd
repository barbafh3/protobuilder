extends Node2D

var state_name: String
var woodcutter = null


func enter_state(parent):
  woodcutter = parent
  state_name = "WoodcutterCarrying"
  parent.get_node("AnimationPlayer").play("Idle")
  woodcutter.target_position = woodcutter.assigned_building.position


func process(delta : float):
  if woodcutter == null:
    woodcutter = get_parent()
  if woodcutter.capacity > 0:
    woodcutter.target_position = woodcutter.assigned_building.position
    var vector: Vector2 = woodcutter.assigned_building.position - woodcutter.position
    if vector.length() > 1:
      woodcutter.move_and_slide(vector.normalized() * woodcutter.speed)
  else:
    exit_state()


func exit_state():
  woodcutter.change_state(woodcutter.States.Idle)
