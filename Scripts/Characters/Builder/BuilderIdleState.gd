extends Node2D

var state_name: String
var builder = null


func enter_state(parent):
    state_name = "BuilderIdle"
    builder = parent
    builder.is_busy = false
    builder.get_new_target(Constants.OUTDOOR_IDLE_RADIUS)
    builder.get_node("AnimationPlayer").current_animation = "Idle"


func process(delta : float):
	if builder == null:
		builder = get_parent()
	builder.idle_move(delta, Constants.OUTDOOR_IDLE_RADIUS)

	if builder.requested_construction != null:
		exit_state()


func exit_state():
	builder.change_state(builder.States.Working)