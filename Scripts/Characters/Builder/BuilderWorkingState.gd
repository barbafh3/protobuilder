extends Node2D

var state_name: String
var builder = null


func enter_state(parent):
	state_name = "BuilderWorking"
	builder = parent
	builder.is_busy = true
	builder.target_position = builder.requested_construction.position


func process(delta : float):
	if builder == null:
		builder = get_parent()
	if (builder.requested_construction != null
			and builder.is_inside_building
			and builder.requested_construction == builder.current_construction):
		if builder.facing == builder.Direction.Left:
			builder.get_node("AnimationPlayer").current_animation = "WorkingLeft"
		if builder.facing == builder.Direction.Right:
			builder.get_node("AnimationPlayer").current_animation = "WorkingRight"
		builder.requested_construction.do_building_work(builder.construction_tick * delta)
	else:
		var vector: Vector2 = builder.target_position - builder.position
		if vector.length() > 1.0:
			builder.move_and_slide(vector.normalized() * builder.speed)
	if builder.requested_construction.construction_remaining <= 0 or builder.requested_construction == null:
		exit_state()


func exit_state():
	builder.clear_building()
	builder.velocity = Vector2.ZERO
	builder.change_state(builder.States.Idle)
