extends "res://Scripts/Abstracts/Mobile.gd"

signal work_finished()

var loading_data = null

# var data_id: String = ""

export var construction_tick = 1.0

var requested_construction = null
var current_construction = null
var current_task = null
var is_busy : bool = false
var is_inside_building : bool = false
var idle_point : Vector2 = Constants.IDLE_POINT

func _ready() -> void:
	VillagerManager.register_villager(self)
	change_state(States.Idle)


func _process(delta):
	update_facing_direction()
	run_state_process(delta)


func _physics_process(delta):
	run_state_physics_process(delta)


func clear_building():
	requested_construction = null
	# current_task = null


func save_data(file):
	# if data_id == "":
	# 	data_id = String(get_instance_id())
	var construction
	var current
	var task
	var state: int = current_state
	if requested_construction != null:
		construction = requested_construction.data_id
	else:
		construction = null
	if current_construction != null:
		current = current_construction.data_id
	else:
		current = null
	if current_task != null:
		task = current_task.data_id
	else:
		task = null
	var data: Dictionary = {
		"data_id": data_id,
		"scene": "res://Scenes/Characters/Builder.tscn",
		"type": "villager",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y
		},
		"state": state,
		"max_idle_movement_timer": max_idle_movement_timer,
		"min_idle_movement_timer": min_idle_movement_timer,
		"local_idle_point": {
			"x": local_idle_point.x,
			"y": local_idle_point.y
		},
		"vector": {
			"x": vector.x,
			"y": vector.y
		},
		"target_position": {
			"x": target_position.x,
			"y": target_position.y,
		},
		"velocity": {
			"x": velocity.x,
			"y": velocity.y,
		},
		"idle_movement_timer": idle_movement_timer,
		"construction_tick": construction_tick,
		"current_task": task,
		"requested_construction": construction,
		"current_construction": current,
		"is_busy": is_busy,
		"is_inside_building": is_inside_building,
		"idle_point": {
			"x": idle_point.x,
			"y": idle_point.y,
		},
	}
	file.store_line(to_json(data))


func load_data(data):
	data_id = data.data_id
	is_loaded = true
	global_position = Vector2(data.global_position.x, data.global_position.y)
	change_state(data.state)
	max_idle_movement_timer = data.max_idle_movement_timer
	min_idle_movement_timer = data.min_idle_movement_timer
	local_idle_point = Vector2(data.local_idle_point.x, data.local_idle_point.y)
	vector = Vector2(data.vector.x, data.vector.y)
	target_position = Vector2(data.target_position.x, data.target_position.y)
	velocity = Vector2(data.velocity.x, data.velocity.y)
	idle_movement_timer = data.idle_movement_timer
	construction_tick = data.construction_tick
	is_busy = data.is_busy
	is_inside_building = data.is_inside_building
	idle_point = Vector2(data.idle_point.x, data.idle_point.y)
	if data.current_task != null:
		current_task = GameManager.get_node_by_id(data.current_task)
	if data.requested_construction != null:
		requested_construction = GameManager.get_node_by_id(data.requested_construction)
	if data.current_construction != null:
		current_construction = GameManager.get_node_by_id(data.current_construction)


# func load_task_data():
# 	if loading_data != null:
# 		if loading_data.current_task != null:
# 			current_task = GameManager.get_node_by_id(loading_data.current_task)
# 		if loading_data.requested_construction != null:
# 			requested_construction = GameManager.get_node_by_id(loading_data.requested_construction)
# 		if loading_data.current_construction != null:
# 			current_construction = GameManager.get_node_by_id(loading_data.current_construction)
# 	else:
# 		print("Builder: Failed to load task data")
