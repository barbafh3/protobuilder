extends Node2D

var state_name: String
var stonecutter = null


func enter_state(parent):
	state_name = "StonecutterIdle"
	stonecutter = parent
	stonecutter.work_duration = stonecutter.base_work_duration
	print("StonecutterIdle: Entering idle state...")
	stonecutter.get_new_target(16)
	stonecutter.get_node("AnimationPlayer").play("Idle")


func process(delta : float):
	if stonecutter == null:
		stonecutter = get_parent()
	if stonecutter.assigned_building != null:
		if stonecutter.assigned_building.has_storage_available:
			stonecutter.current_stones = stonecutter.assigned_building.get_nearest_resources()
			exit_state()
	stonecutter.idle_move(delta, Constants.INDOOR_IDLE_RADIUS)


func exit_state():
	stonecutter.change_state(stonecutter.States.Working)

