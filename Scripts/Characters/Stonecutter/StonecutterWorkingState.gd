extends Node2D

var target_position: Vector2
var state_name: String
var stonecutter = null
var current_stones = null
var has_reserved_resources: bool = false


func enter_state(parent):
  state_name = "StonecutterWorking"
  stonecutter = parent
  current_stones = stonecutter.current_stones
  get_new_target()
  has_reserved_resources = false
  if not stonecutter.current_stones.is_connected("resource_spent", self, "on_stones_destroyed"):
    stonecutter.current_stones.connect("resource_spent", self, "on_stones_destroyed")


func process(delta: float):
  if stonecutter == null:
    stonecutter = get_parent()
    current_stones = stonecutter.current_stones
  var has_stones_selected: bool = current_stones != null
  if has_stones_selected:
    stonecutter.target_position = current_stones.position
    var stones_have_enough_resources = current_stones.resource_amount >= stonecutter.max_capacity
    var resources_reserved = current_stones.resource_amount <= current_stones.reserved_amount
    if stones_have_enough_resources:
      if not resources_reserved and not has_reserved_resources:
        current_stones.reserved_amount += stonecutter.max_capacity
        has_reserved_resources = true
      if not stonecutter.is_inside_stones:
        move_to_target()
      else:
        do_work(delta)
    else:
      stonecutter.current_stones = null
      exit_state(stonecutter.States.Idle)
  else:
    exit_state(stonecutter.States.Idle)


func move_to_target():
  stonecutter.get_node("AnimationPlayer").play("Idle")
  var vector: Vector2 = \
    stonecutter.current_stones.global_position - stonecutter.global_position
  if vector.length() > 1.0:
    stonecutter.move_and_slide(vector.normalized() * stonecutter.speed)


func do_work(delta):
  var animation = "WorkingRight"
  if stonecutter.facing == stonecutter.Direction.Left:
    animation = "WorkingLeft"
  stonecutter.get_node("AnimationPlayer").play(animation)
  work_tick(delta)
  if stonecutter.work_duration <= 0:
    if assigned_building_has_capacity():
      stonecutter.capacity = current_stones.deliver_resource(stonecutter)
      current_stones.reserved_amount -= stonecutter.max_capacity
      exit_state(stonecutter.States.Carrying)
    else:
      exit_state(stonecutter.States.Idle)


func work_tick(delta: float):
  if stonecutter.work_duration > 0:
    stonecutter.work_duration -= delta


func assigned_building_has_capacity() -> bool:
  return stonecutter.assigned_building.capacity < stonecutter.assigned_building.max_capacity


func get_new_target():
  var rand_x = rand_range(stonecutter.current_stones.global_position.x - stonecutter.target_radius,
              stonecutter.current_stones.global_position.x + stonecutter.target_radius)
  var rand_y = rand_range(stonecutter.current_stones.global_position.y - stonecutter.target_radius,
              stonecutter.current_stones.global_position.y + stonecutter.target_radius)
  target_position = Vector2(rand_x, rand_y)


func on_stones_destroyed(stones):
  stonecutter.current_stones = null
  stonecutter.is_inside_stones = false


func exit_state(state):
  stonecutter.is_loaded = false
  stonecutter.change_state(state)
