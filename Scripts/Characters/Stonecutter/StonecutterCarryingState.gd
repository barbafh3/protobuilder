extends Node2D

var state_name: String
var stonecutter = null


func enter_state(parent):
	stonecutter = parent
	state_name = "StonecutterCarrying"
	parent.get_node("AnimationPlayer").play("Idle")
	stonecutter.target_position = stonecutter.assigned_building.position


func process(delta : float):
	if stonecutter == null:
		stonecutter = get_parent()
	if stonecutter.capacity > 0:
		var vector: Vector2 = stonecutter.assigned_building.position - stonecutter.position
		if vector.length() > 1:
			stonecutter.move_and_slide(vector.normalized() * stonecutter.speed)
	else:
		exit_state()


func exit_state():
	stonecutter.change_state(stonecutter.States.Idle)
