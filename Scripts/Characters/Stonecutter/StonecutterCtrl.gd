extends "res://Scripts/Abstracts/Mobile.gd"

export var base_work_duration: float = 0.0
export var max_capacity: int = 0
export var target_radius: float = 0.0

var crate_scene: PackedScene = preload("res://Scenes/Resources/Crate.tscn")
var loading_data = null

var work_duration: float = 0
var capacity: int = 0
var assigned_building = null
var idle_point: Vector2 = Vector2.ZERO
var is_busy: bool = false
var current_stones = null
var is_inside_stones: bool = false


func _ready() -> void:
  work_duration = base_work_duration
  change_state(States.Idle)


func _process(delta: float) -> void:
  if assigned_building == null:
    idle_point = Constants.IDLE_POINT
  update_facing_direction()
  run_state_process(delta)
  if assigned_building == null:
    drop_inventory_and_revert_to_villager()


func _physics_process(delta):
  run_state_physics_process(delta)


func deliver_resources():
  var delivered_amount: int = capacity
  capacity = 0
  return { "type" : Constants.Resources.Stone, "amount" : delivered_amount }


func set_building(building : Node2D):
  assigned_building = building
  local_idle_point = assigned_building.position


func connect_to_stones():
  current_stones.connect("resource_spent", self, "change_current_stones")


func change_current_stones(trees):
  current_stones.disconnect("resource_spent", self, "change_current_stones")
  current_stones = assigned_building.get_nearest_stones()
  current_stones.connect("resource_spent", self, "change_current_stones")
  if current_stones == null:
    change_state(States.Idle)


func save_data(file):
  var stones
  if current_stones != null:
    stones = current_stones.data_id
    print("Stonecutter: Saving stones ", stones)
  else:
    print("Stonecutter: Cant save stones")
    stones = null
  var state: int = current_state
  var data: Dictionary = {
    "data_id": data_id,
    "scene": "res://Scenes/Characters/Stonecutter.tscn",
    "type": "villager",
    "global_position": {
      "x": global_position.x,
      "y": global_position.y
    },
    "state": state,
    "max_idle_movement_timer": max_idle_movement_timer,
    "min_idle_movement_timer": min_idle_movement_timer,
    "local_idle_point": {
      "x": local_idle_point.x,
      "y": local_idle_point.y
    },
    "idle_point": {
      "x": idle_point.x,
      "y": idle_point.y
    },
    "vector": {
      "x": vector.x,
      "y": vector.y
    },
    "target_position": {
      "x": target_position.x,
      "y": target_position.y,
    },
    "velocity": {
      "x": velocity.x,
      "y": velocity.y,
    },
    "idle_movement_timer": idle_movement_timer,
    "work_duration": work_duration,
    "capacity": capacity,
    "assigned_building": assigned_building.data_id,
    "is_busy": is_busy,
    "is_inside_stones": is_inside_stones,
    "current_stones": stones
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  is_loaded = true
  global_position = Vector2(data.global_position.x, data.global_position.y)
  max_idle_movement_timer = data.max_idle_movement_timer
  min_idle_movement_timer = data.min_idle_movement_timer
  local_idle_point = Vector2(data.local_idle_point.x, data.local_idle_point.y)
  idle_point = Vector2(data.idle_point.x, data.idle_point.y)
  vector = Vector2(data.vector.x, data.vector.y)
  target_position = Vector2(data.target_position.x, data.target_position.y)
  velocity = Vector2(data.velocity.x, data.velocity.y)
  idle_movement_timer = data.idle_movement_timer
  work_duration = data.work_duration
  capacity = data.capacity
  assigned_building = GameManager.get_node_by_id(data.assigned_building)
  is_busy = data.is_busy
  is_inside_stones = data.is_inside_stones
  if data.current_stones != null:
    current_stones = GameManager.get_node_by_id(data.current_stones)
  change_state(data.state)


func drop_inventory_and_revert_to_villager():
  if capacity > 0:
    var crate = crate_scene.instance()
    crate.storage = { Constants.Resources.Stone: capacity }
    crate.global_position = global_position
    get_node("/root/Map/Resources").add_child(crate)
    StorageManager.register_storage(crate)
  VillagerManager.revert_to_villager(self)
