extends Node2D

var state_name: String
var hauler = null


func enter_state(parent):
  hauler = parent
  state_name = "HaulerLoading"
  hauler.is_busy = true


func process(delta: float) -> void:
  if hauler == null:
    hauler = get_parent()
  if hauler.loading_failed:
    exit_state(true)
  if hauler.is_inside_building:
    if hauler.loading_timeout > 0.0:
      hauler.loading_timeout -= delta
    else:
      hauler.loading_failed = true
  if hauler.resource_origin == null:
    hauler.get_new_source()
  if hauler.resource_destination == null:
    hauler.return_resources()
  if hauler.capacity <= 0 and hauler.resource_origin != null:
    hauler.target_position = hauler.resource_origin.global_position
    var vector: Vector2 = hauler.resource_origin.global_position - hauler.global_position
    if vector.length() > 1.0:
      hauler.move_and_slide(vector.normalized() * hauler.speed)
  else:
    exit_state()


func loading_timeout_tick(delta: float):
  if hauler.loading_timeout > 0.0:
    hauler.loading_timeout -= delta


func exit_state(to_idle: bool = false):
  if to_idle:
    hauler.emit_signal("hauling_finished", self, 0, hauler.amount_requested)
    hauler.amount_requested = 0
    hauler.resource_destination = null
    hauler.loading_timeout = hauler.base_loading_timeout
    hauler.loading_failed = false
    hauler.change_state(hauler.States.Idle)
  else:
    hauler.change_state(hauler.States.Carrying)
