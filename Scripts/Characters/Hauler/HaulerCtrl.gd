extends "res://Scripts/Abstracts/Mobile.gd"

signal hauling_finished(hauler, amount_delivered)

var loading_data = null
# var data_id: String = ""

export var max_capacity: int = 10
export var base_loading_timeout: float = 0.3

var capacity: int = 0
var item_type: String = ""
var amount_requested: int = 0
var idle_point: Vector2 = Constants.IDLE_POINT
var is_busy: bool = false
var resource_origin: Node2D = null
var resource_destination: Node2D = null
var current_task = null
var loading_timeout: float = 0.3
var loading_failed: bool = false
var is_inside_building: bool = false


func _ready() -> void:
  VillagerManager.register_villager(self)
  change_state(States.Idle)
  $Label.text = name


func _process(delta):
  if current_state == States.None:
    current_state = States.Idle
  update_facing_direction()
  check_for_destroyed_buildings()
  run_state_process(delta)


func _physics_process(delta):
  run_state_physics_process(delta)


func deliver_resources():
  var delivered_amount = capacity
  capacity = 0
  emit_signal("hauling_finished", self, delivered_amount)
  return {"type": item_type, "amount": delivered_amount}


func set_resource_origin(origin: Node2D):
  resource_origin = origin


func set_resource_destination(destination: Node2D):
  resource_destination = destination


func check_for_destroyed_buildings():
  if resource_origin == null and current_state != States.Idle:
    if amount_requested > 0:
      get_new_source()
  if resource_destination == null and current_state != States.Idle:
    return_resources()


func get_new_source():
  var source = StorageManager.request_source_for_item(item_type, amount_requested)
  set_resource_origin(source)


func return_resources():
  var warehouse = StorageManager.request_storage(item_type, capacity)
  resource_destination = warehouse


func save_data(file):
  var origin
  var destination
  var task
  var state: int = current_state
  if resource_origin != null:
    origin = resource_origin.data_id
  else:
    origin = null
  if resource_destination != null:
    destination = resource_destination.data_id
  else:
    destination = null
  if current_task != null:
    task = current_task.data_id
  else:
    task = null
  var data: Dictionary = {
    "data_id": data_id,
    "scene": "res://Scenes/Characters/Hauler.tscn",
    "type": "villager",
    "global_position": {
      "x": global_position.x,
      "y": global_position.y
    },
    "state": state,
    "max_idle_movement_timer": max_idle_movement_timer,
    "min_idle_movement_timer": min_idle_movement_timer,
    "local_idle_point": {
      "x": local_idle_point.x,
      "y": local_idle_point.y
    },
    "vector": {
      "x": vector.x,
      "y": vector.y
    },
    "target_position": {
      "x": target_position.x,
      "y": target_position.y,
    },
    "velocity": {
      "x": velocity.x,
      "y": velocity.y,
    },
    "idle_movement_timer": idle_movement_timer,
    "capacity": capacity,
    "item_type": item_type,
    "amount_requested": amount_requested,
    "current_task": task,
    "is_busy": is_busy,
    "resource_origin": origin,
    "resource_destination": destination,
    "loading_timeout": loading_timeout,
    "loading_failed": loading_failed,
    "is_inside_building": is_inside_building
  }
  file.store_line(to_json(data))


func load_data(data):
  data_id = data.data_id
  is_loaded = true
  global_position = Vector2(data.global_position.x, data.global_position.y)
  change_state(data.state)
  max_idle_movement_timer = data.max_idle_movement_timer
  min_idle_movement_timer = data.min_idle_movement_timer
  local_idle_point = Vector2(data.local_idle_point.x, data.local_idle_point.y)
  vector = Vector2(data.vector.x, data.vector.y)
  target_position = Vector2(data.target_position.x, data.target_position.y)
  idle_movement_timer = data.idle_movement_timer
  velocity = Vector2(data.velocity.x, data.velocity.y)
  is_busy = data.is_busy
  loading_timeout = data.loading_timeout
  loading_failed = data.loading_failed
  is_inside_building = data.is_inside_building
  if data.current_task != null:
    current_task = GameManager.get_node_by_id(data.current_task)
  capacity = data.capacity
  item_type = data.item_type
  amount_requested = data.amount_requested
  if data.resource_origin != null:
    resource_origin = GameManager.get_node_by_id(data.resource_origin)
  if data.resource_destination != null:
    resource_destination = GameManager.get_node_by_id(data.resource_destination)
