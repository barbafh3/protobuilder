extends Node2D

var state_name: String
var hauler = null

func enter_state(parent):
	hauler = parent
	state_name = "HaulerIdle"
	hauler.set_resource_destination(null)
	if hauler.resource_origin != null:
		if hauler.resource_origin.is_connected("building_destroyed", hauler, "on_source_destroyed"):
			hauler.resource_origin.disconnect("building_destroyed", hauler, "on_source_destroyed")
		hauler.set_resource_origin(null)
	hauler.is_busy = false
	hauler.get_new_target(Constants.OUTDOOR_IDLE_RADIUS)
	hauler.is_loaded = false


func physics_process(delta: float) -> void:
    if hauler == null:
        hauler = get_parent()
    hauler.idle_move(delta, Constants.OUTDOOR_IDLE_RADIUS)
    if hauler.amount_requested > 0:
        exit_state(hauler.States.Loading)
    elif hauler.resource_destination != null:
        exit_state(hauler.States.Carrying)


func exit_state(state):
	hauler.change_state(state)
