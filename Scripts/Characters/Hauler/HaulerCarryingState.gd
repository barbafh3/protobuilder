extends Node2D

var state_name: String
var hauler = null
var _amount : int = 0


func enter_state(parent):
	hauler = parent
	state_name = "HaulerCarrying"
	_amount = hauler.capacity


func physics_process(delta: float) -> void:
	if hauler == null:
		hauler = get_parent()
	if hauler.resource_destination != null:
		hauler.target_position = hauler.resource_destination.global_position
		if hauler.capacity > 0:
			var vector: Vector2 = hauler.resource_destination.position - hauler.position
			if vector.length() > 1.0:
				hauler.move_and_slide(vector.normalized() * hauler.speed)
		else:
			exit_state()
	else:
		hauler.resource_destination = StorageManager.request_storage(hauler.item_type, hauler.capacity)



func exit_state():
	hauler.amount_requested = 0
	hauler.velocity = Vector2.ZERO
	hauler.change_state(hauler.States.Idle)
