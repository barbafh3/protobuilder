extends "res://Scripts/Abstracts/Mobile.gd"

var loading_data = null

var is_busy : bool = true

func _ready() -> void:
	VillagerManager.register_villager(self)


func _process(delta: float) -> void:
	idle_move(delta, Constants.OUTDOOR_IDLE_RADIUS)


func save_data(file):
	var data = {
		"data_id": data_id,
		"scene": "res://Scenes/Characters/Villager.tscn",
		"type": "villager",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y
		}
	}

	file.store_line(to_json(data))


func load_data(data):
	data_id = String(data.data_id)
	global_position = Vector2(data.global_position.x, data.global_position.y)
