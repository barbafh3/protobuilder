extends Node2D

var state_name: String
var carpenter = null


func enter_state(parent):
	state_name = "CarpenterIdle"
	carpenter = parent
	carpenter.work_duration = carpenter.base_work_duration
	carpenter.get_new_target(16)
	carpenter.get_node("AnimationPlayer").play("Idle")


func process(delta : float):
	if carpenter == null:
		carpenter = get_parent()
	if carpenter.assigned_building != null:
		var sawmill_has_enough_wood = \
			carpenter.assigned_building.input_storage >= carpenter.wood_per_plank
		var sawmill_has_storage_space = carpenter.assigned_building.has_storage_available

		if sawmill_has_storage_space and sawmill_has_enough_wood:
			exit_state()
		carpenter.idle_move(delta, Constants.INDOOR_IDLE_RADIUS)


func exit_state():
	carpenter.is_loaded = false
	carpenter.change_state(carpenter.States.Working)
