extends Node2D

var state_name: String
var carpenter = null


func enter_state(parent):
	state_name = "CarpenterWorking"
	carpenter = parent


func process(delta : float):
	if carpenter == null:
		carpenter = get_parent()
	if carpenter.assigned_building != null:
		carpenter.target_position = carpenter.assigned_building.position
	if carpenter.is_inside_building:
		if carpenter.facing == carpenter.Direction.Left:
			carpenter.get_node("AnimationPlayer").current_animation = "WorkingLeft"
		if carpenter.facing == carpenter.Direction.Right:
			carpenter.get_node("AnimationPlayer").current_animation = "WorkingRight"
		var sawmill_storage_plus_new_plank: int = carpenter.assigned_building.output_storage + 1
		var sawmill_has_enough_wood: bool = \
			carpenter.assigned_building.input_storage >= carpenter.wood_per_plank
		var sawmill_output_has_space: bool = \
			sawmill_storage_plus_new_plank <= carpenter.assigned_building.max_capacity

		var can_make_more_planks: bool = sawmill_has_enough_wood and sawmill_output_has_space
		if can_make_more_planks:
			if carpenter.work_duration > 0:
				carpenter.work_duration -= delta
			else:
				carpenter.work_duration = carpenter.base_work_duration
				carpenter.assigned_building.input_storage -= carpenter.wood_per_plank
				carpenter.assigned_building.output_storage += 1
				carpenter.assigned_building.spawn_text("1")
		else:
			exit_state()
	else:
		carpenter.get_node("AnimationPlayer").play("Idle")
		var vector: Vector2 = carpenter.assigned_building.global_position - carpenter.global_position
		carpenter.move_and_slide(vector.normalized() * carpenter.speed)


func exit_state():
	carpenter.is_loaded = false
	carpenter.change_state(carpenter.States.Idle)
