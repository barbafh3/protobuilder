extends "res://Scripts/Abstracts/Mobile.gd"

export var base_work_duration: float = 0.0
export var wood_per_plank: int = 0

var loading_data = null

var work_duration: float = 0.0
var assigned_building
var is_inside_building: bool
var is_busy: bool = false


func _ready() -> void:
	change_state(States.Idle)


func _process(delta):
	update_facing_direction()
	run_state_process(delta)


func _physics_process(delta):
	run_state_physics_process(delta)


func set_building(building : Node2D):
	assigned_building = building
	local_idle_point = assigned_building.position


func save_data(file):
	var state: int = current_state
	var data: Dictionary = {
		"data_id": data_id,
		"scene": "res://Scenes/Characters/Carpenter.tscn",
		"type": "villager",
		"global_position": {
			"x": global_position.x,
			"y": global_position.y
		},
		"state": state,
		"max_idle_movement_timer": max_idle_movement_timer,
		"min_idle_movement_timer": min_idle_movement_timer,
		"local_idle_point": {
			"x": local_idle_point.x,
			"y": local_idle_point.y
		},
		"vector": {
			"x": vector.x,
			"y": vector.y
		},
		"target_position": {
			"x": target_position.x,
			"y": target_position.y,
		},
		"velocity": {
			"x": velocity.x,
			"y": velocity.y,
		},
		"idle_movement_timer": idle_movement_timer,
		"work_duration": work_duration,
		"assigned_building": assigned_building.data_id,
		"is_inside_building": is_inside_building,
		"is_busy": is_busy,
	}
	file.store_line(to_json(data))


func load_data(data):
	data_id = data.data_id
	is_loaded = true
	change_state(data.state)
	global_position = Vector2(data.global_position.x, data.global_position.y)
	max_idle_movement_timer = data.max_idle_movement_timer
	min_idle_movement_timer = data.min_idle_movement_timer
	local_idle_point = Vector2(data.local_idle_point.x, data.local_idle_point.y)
	vector = Vector2(data.vector.x, data.vector.y)
	target_position = Vector2(data.target_position.x, data.target_position.y)
	velocity = Vector2(data.velocity.x, data.velocity.y)
	idle_movement_timer = data.idle_movement_timer
	work_duration = data.work_duration
	assigned_building = GameManager.get_node_by_id(data.assigned_building)
	is_inside_building = data.is_inside_building
	is_busy = data.is_busy

